**TODO LIST**
Every item contains a 0 or !0.  **Poc** is considered done if no item contains !0.

- 0   make: 4 square show images.
- 0   make: reset.
- 0   make: 1more row with 4 square show images.
- 0   make: controller avaible+show matching images of given pressed buttons
- 0   make: Find out Game.
- 0   make: CI/CD.
- 0   make: Keyboard fallback 
- 0   make: Behavior
- 0   add: SWIG.
- !0+ add: Python3.
- !0+ Create Interfaces.
- !0+ Implement Interfaces.
- !0- make: Ctest. now: mainly using built-in stuffs
- !0  Network bohockodas
- !0  no leak.
- !0  add: Robot.


Note:
- For dark mode exec: `./poc -style=kvantum`
