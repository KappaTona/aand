#ifndef SWIG_WRAPPER
#define SWIG_WRAPPER
#include <memory>


class SwigWrapper
{
public:
    virtual ~SwigWrapper();

    static std::shared_ptr<SwigWrapper> create();
    virtual void start()=0;
    virtual void end()=0;
};


#endif
