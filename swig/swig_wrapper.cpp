#include "swig_wrapper.h"
#include <QApplication>
#include <iostream>
#include <view.h>


SwigWrapper::~SwigWrapper()=default;

class ImplSwigWrapper: public SwigWrapper
{
    int* fake_argc = new int{1};
    QApplication* main_app_ = new QApplication{*fake_argc, nullptr};
    View* main_view_ = new View{{860, 480}};
public:
    void start() override;
    void end() override;
};

std::shared_ptr<SwigWrapper> SwigWrapper::create()
{
    return std::make_shared<ImplSwigWrapper>();
}

void ImplSwigWrapper::start()
{
    auto exit_code = main_app_->exec();
    while(exit_code)
    {
        std::cout<< "exit_code:" << exit_code << std::endl;
        main_view_->restart();
        exit_code = main_app_->exec();
    }
    end();
}

void ImplSwigWrapper::end()
{
    delete main_view_;
    delete main_app_;
}
