add_library(base_frame SHARED
    connector.cpp
    present_scene.cpp
    scene.cpp
    view.cpp

    ${CMAKE_SOURCE_DIR}/res/poc.qrc
)

add_executable(poc
    main.cpp
)

target_link_libraries(base_frame Qt5::Widgets Qt5::Gamepad)
add_subdirectory(layout)
target_link_libraries(poc base_frame layout)
