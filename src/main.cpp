#include <QApplication>
#include <iostream>
#include "view.h"


int main(int argc, char** argv)
{
    auto* app = new QApplication{argc, argv};
    auto* main_dialog_view = new View{{860, 480}};

    auto exit_code = app->exec();
    while (exit_code)
    {
        std::cout << "exit_code:" << exit_code << std::endl;
        main_dialog_view->restart();
        exit_code = app->exec();
    }

    delete main_dialog_view;
    delete app;
    return 0;
}
