#pragma once
#include <QDialog>
#include <QPoint>
class Scene;
class View: public QDialog
{
    Q_OBJECT
    Scene* main_scene_;
public:
    View(QPoint bound_limits);
    void restart();
    void start();
};
