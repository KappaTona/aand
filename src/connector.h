#ifndef CONNECTOR_H
#define CONNECTOR_H
#include <QObject>
class QString;


class Connector: public QObject
{
    Q_OBJECT
public:
    explicit Connector(QObject* parent=nullptr);

Q_SIGNALS:
    void pressed(QString direction);

private:
    void emit_pressed(QString direction);
};


#endif
