#include "present_scene.h"
#include <QApplication>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QPushButton>
#include <QResource>
#include <QString>


PresentScene::PresentScene(QWidget* parent) : Scene{parent}
{
    QResource::registerResource("res/poc.qrc");

    auto* control_layout = new QHBoxLayout;
    control_layout->addWidget(special_button(tr("&Restart"), 777));
    control_layout->addWidget(special_button(tr("&Exit"), 0));

    auto* grid_layout = new QGridLayout;
    grid_layout->addLayout(display_layout_.get(), 1, 0);
    grid_layout->addLayout(gameplay_layout_.get(), 2, 0);
    grid_layout->addLayout(control_layout, 3, 0);
    connect(change_image_with_.get(), &Connector::pressed,
            this, &PresentScene::change);

    setLayout(grid_layout);
    show();
}

QPushButton* PresentScene::special_button(QString text, int code)
{
    auto* special_button = new QPushButton;
    special_button->setText(text);
    special_button->setBaseSize(100, 50);
    connect(special_button, &QPushButton::clicked, [code] { qApp->exit(code); });
    return special_button;
}

void PresentScene::change(QString to_what)
{
    if (where_ > 3)
        return;

    gameplay_layout_->change(where_, to_what);
    display_layout_->change(where_, to_what);
    ++where_;
}

bool PresentScene::eventFilter(QObject* obj, QEvent* ev)
{
    if (ev->type() != QEvent::KeyPress)
        return false;

    auto key_event = static_cast<QKeyEvent*>(ev);
    switch (key_event->key())
    {
        case Qt::Key_W: {
            change(":/up");
            return true;
        }
        case Qt::Key_S: {
            change(":/down");
            return true;
        }
        case Qt::Key_A: {
            change(":/left");
            return true;
        }
        case Qt::Key_D: {
            change(":/right");
            return true;
        }
        default:
            return QWidget::eventFilter(obj, ev);
    }
    return false;
}
