#pragma once
#include "scene.h"
#include <memory>
#include "connector.h"
#include "layout/display.h"
#include "layout/gameplay.h"
class QPushButton;
class QString;
class PresentScene: public Scene
{
    Q_OBJECT

    std::unique_ptr<Connector> change_image_with_ = std::make_unique<Connector>();
    std::shared_ptr<layout::Gameplay> gameplay_layout_ = std::make_shared<layout::Gameplay>();
    std::shared_ptr<layout::Display> display_layout_ = std::make_shared<layout::Display>();
    int where_ = 0;

public:
    PresentScene(QWidget* parent=nullptr);

protected:
    bool eventFilter(QObject* obj, QEvent* ev) override;

private:
    QPushButton* special_button(QString name, int code);
    void change(QString to_what);
};
