#include "view.h"
#include "present_scene.h"


View::View(QPoint bound_limits)
{
    setFixedWidth(bound_limits.x());
    setFixedHeight(bound_limits.y());
    start();
    show();
}

void View::restart()
{
    removeEventFilter(main_scene_);
    delete main_scene_;
    start();
}

void View::start()
{
    main_scene_ = new PresentScene{this};
    installEventFilter(main_scene_);
}
