#   Connector: public QObject


**Constructor**
`explicit Connector(QObject* parent=nullptr);`
- Using QGamepadManage searches for active gamepads.
- Using the first one. Creates the connection between gamepad and pressed signal.


**Q_SIGNALS**
`void pressed(QString direction);`
- Used for signaling that one of the four controller button is pressed.


Example:
- when ![":/up"](res/y+1.jpg) button is pressed, signal is emitted.


#   DisplayLayout: public InterfaceLayout


**Constructor**
`DisplayLayout(const std::vector<QString>& order);`
- Creates 4 ![":/qmark"](res/ping.png) **Image** top row.


`Image* image_at(int where) override;`
&
`void change(int where, QString to);`

Example:
- `int where = 2; QString to = ":/good"`
- `image_at(where)->change(to);`
- This will change the third image on top row to ![":/good"](res/default.png)


#   GameplayLayout: public InterfaceLayout


**Constructor**
`GameplayLayout();`
- Creates 4 ![":/qmark"](res/ping.png) **Image** mid row.


`Image* image_at(int where)`
Example:
- `int where = 0;` returns pointer to first **Image** in mid row.


#   Image: public QLabel


**Constructor** `Image(QString of)`
- calls `change(of);`


`void change(QString to_what);`
- Uses base class **QLabel**::{setPixmap, setBaseSize};


#   InterfaceLayout: public QHBoxLayout


**Destructor** `virtual InterfaceLayout()=default`


`virtual Image* image_at(int where)=0;`


Notes:
- prolly would be better to change InterfaceLayout 


#   PresentScene: public Scene


**Constructor** `PresentScene(QWidget* parent=nullptr);`


`protected: bool eventFilter(QObject* obj, QEvent* ev) override;`
- Used for catching w-a-s-d from *QEvent::KeyPress*


PresentScene rough summary:
- **View::{start, restart}**
- **PresentScene** uses: **Connector**, **GameplayLayout**, **DisplayLayout**


While creating:
- shuffles a random resource string chain of four, for us, to guess later.
- creates *QGridLayout* with 
    - 4 column, 2 rows: top row is **DisplayLayout**, mid row is **GameplayLayout**
    - bottom row: restart, exit buttons
- creates connection between **Connector::pressed**, **PresentScene::change**


While behaving:

Let us imagine a grid with 3 rows. Top row is where you
expect the ![":/good"](res/default.png) while you press proper buttons.

Following with that, Mid row is where you see what you are doing
when pressing buttons.

Bottom row is where the exit and restart buttons are.


#   How?

At this point we should have a basic concept of this little code.

This little code is a find out game. It is possible to guess
the right order anytime, it's just hard.

You will see unequivocal results as of True and False. In the
image of ![":/good"](res/default.png) and ![":/wrong"](res/wrong.png).

Recommended best: top row is filled with good guesses, while we see
our actions mid row clearly.


Having fun is good.


Example:

- Start
![":/start-layout"](res/start-layout.png)

- Victory Royale
![":/victory-royale"](res/victory-royale.png)

- Nice try
![":/nice-try"](res/nice-try.png)



