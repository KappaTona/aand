#pragma once
#include <memory>
#include <vector>
#include "image.h"
class QString;


namespace layout {
using images_t = std::vector<std::shared_ptr<Image>>;

class JointBehavior
{
public:
    virtual ~JointBehavior();

protected:
    images_t make_default_images(size_t count, QString source);
    images_t make_images(std::vector<QString>&& sources);
    std::vector<QString> shuffle_strings(std::vector<QString>&& from);
};


}
