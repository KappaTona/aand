#include "image.h"
#include <QString>


namespace layout {
Image::Image(QString of)
{
    change(of);
}

void Image::change(QString to_what)
{
    setPixmap(QPixmap::fromImage(QImage{to_what}));
    setBaseSize(112, 112);
}


}
