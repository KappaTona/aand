#pragma once
#include <memory>
#include <vector>
#include "image.h"
#include "joint_behavior.h"
#include "layout.h"
class QString;


namespace layout {
class Display: public Layout, protected JointBehavior
{
    Q_OBJECT

    std::vector<std::shared_ptr<Image>> images_;
    std::vector<QString> to_guess_;

public:
    Display();

    void change(int where, QString to) override;
};


}
