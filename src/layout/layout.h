#pragma once
#include <QHBoxLayout>
class QString;


namespace layout {
class Layout: public QHBoxLayout
{
    Q_OBJECT
public:
    virtual ~Layout();

    virtual void change(int where, QString to_what)=0;
};


}
