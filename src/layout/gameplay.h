#pragma once
#include <memory>
#include <vector>
#include "image.h"
#include "joint_behavior.h"
#include "layout.h"
class QString;


namespace layout {
class Gameplay: public Layout, protected JointBehavior
{
    Q_OBJECT

    std::vector<std::shared_ptr<Image>> images_;
public:
    Gameplay();

    void change(int where, QString to_what) override;
};


}
