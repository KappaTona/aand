#include "gameplay.h"


namespace layout {
Gameplay::Gameplay()
{
    auto to = make_default_images(4, ":/qmark");
    images_.swap(to);
    for (auto each: images_)
        addWidget(each.get());
}

void Gameplay::change(int where, QString to_what)
{
    images_[where]->change(to_what);
}


}
