#pragma once
#include <QLabel>
class QString;


namespace layout {
class Image: public QLabel
{
    Q_OBJECT

public:
    Image(QString of);

public Q_SLOT:
    void change(QString to_what);

};


}
