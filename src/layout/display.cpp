#include "display.h"


namespace layout {
Display::Display()
{
    to_guess_ = shuffle_strings({":/up", ":/down", ":/left", ":/right"});
    auto to = make_default_images(4, ":/qmark");
    images_.swap(to);
    for (auto each: images_)
        addWidget(each.get());
}

void Display::change(int where, QString to)
{
    images_[where]->change(to == to_guess_[where] ?
            ":/good" : ":/wrong");
}


}
