#include "joint_behavior.h"
#include <QString>
#include <algorithm>
#include <random>


namespace layout {
JointBehavior::~JointBehavior()=default;


images_t JointBehavior::make_default_images(size_t count, QString source)
{
   images_t images = {count, nullptr}; 
   std::generate(images.begin(), images.end(),
           [&]{ return std::make_shared<Image>(source); });
   return std::move(images);
}

images_t JointBehavior::make_images(std::vector<QString>&& sources)
{
    images_t images;
    std::generate(images.begin(), images.end(), [&]{
        static size_t i = 0;
        return std::make_shared<Image>(sources[i++]);
    });
    return std::move(images);
}

std::vector<QString> JointBehavior::shuffle_strings(std::vector<QString>&& from)
{
    auto to = from;
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(to.begin(), to.end(), g);
    return std::move(to);
}


}
