#include "connector.h"
#include <QDebug>
#include <QGamepad>
#include <QString>


Connector::Connector(QObject* parent)
{
    auto gamepads = QGamepadManager::instance()->connectedGamepads();
    if (gamepads.isEmpty())
        qDebug() << "no controller found. Use W-A-S-D instead.\n";

    auto* gamepad = new QGamepad{*gamepads.begin(), this};
    connect(gamepad, &QGamepad::buttonYChanged, [this](bool pressed){
            if (pressed)
                emit_pressed({":/left"});
        });
    connect(gamepad, &QGamepad::buttonAChanged, [this](bool pressed){
            if (pressed)
                emit_pressed({":/down"});
        });
    connect(gamepad, &QGamepad::buttonXChanged, [this](bool pressed){
            if (pressed)
                emit_pressed({":/up"});
        });
    connect(gamepad, &QGamepad::buttonBChanged, [this](bool pressed){
            if (pressed)
                emit_pressed({":/right"});
        });
}

void Connector::emit_pressed(QString direction)
{
    Q_EMIT pressed(direction);
}
