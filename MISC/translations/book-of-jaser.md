[source](https://www.parsontom.com/books/Book%20of%20Jasher.pdf)
65. 1-37
És ezen dolgok után jött, hogy minden tanácsadója a Fáraónak hát Égyiptom királyának,
és az összes véne Égyiptomnak összegyűltek és a király elé jöttek földig hajoltak és a király
elé ültek.
És a tanácsadók és vének Égyiptomnak beszéltek a király alá, mondván hogy:
Ímé Izráél gyermekei többek és hatalmasabbak mint mi vagyunk, és tudjuk a sok
gonoszságot amit velünk tettek, [TL;DR Égyiptomiak felkérték őket csatára és jól otthagyták az
Iráéliakat, ezért Izráél gyermekei a csata után az úton hazafele megölték az Égyiptomi katonákat
akiket láttak. Részletek a 64. fejezetben].
(Folytatták a tanácsadók:) És bizonyára láttuk, hogy mire képesek ezek kisszámban, hát még ha többen lesznek, vajjon mit tesznek?
Na már most, tarcsunk táncsot, hogy mit tegyünk velük hogy fokozottan pusztítsuk őket közülünk,
nehogy túl népesedjenek rajtunk itt Égyiptom földjén. Mert biztos, hogyha az Izráél fiai megnövekednek
országunkban akadállyá válnak, és ha háborúi jönne ránk bizonyára az ellenséghez csatlakoznak, hogy
megszabaduljanak tőlünk.
Erre a Fáraó, Égyiptom királya így felelt a véneknek, Ez lesz a terv Izráél ellen amitől nem térünk el bárhogy is.
Ímé Pithom és Rameses, városok nincsenek megerősítve, és nem tudnák viselni a háborút és nekünk
illene megerősíteni őket és felépíteni őket. 
Most mindazonáltal ravaszul kell cselekednünk feléjük, és határozatott kell törvénybe iktatni Égyiptom
földjén és Goshen földjén parancsolván a király által, mondva:
Minden férfi Égyiptomban, Goshenben. Pathrosban és ezek lakosainak! A király parancsolt felőlünk,
hogy építsük és erősítsük meg Pithom és Rameses városát csatakészre. Bárki közületek Égyiptomiak
és Izráél gyermekei és minden városlakosoknak, akik akarnak velünk építkezni, kapjanak bért naponta
a király parancsára.
Tehát menjetek előre és cselekedjetek ravaszon és gyüjtsétek magotok egybe és menjetek Pithom és
Ramesesbe építkezni.
És miközben építkeztek, hajtsátok végre ezt a rendelkezést minden nap tartsatok be úgyahogy
a király rendelkezett. És amikor valamennyien Izráél gyermekeiből eljönnek hozzátok építkezni,
adjatok nekik bért napokig naponta. És ahogy telnek a napok és ők kapják a fizetésüket továbbra is
napi szinten, ti Égyiptomiak vonjátok ki magotok a munka alól és ne dolgozzatok velük együtt,
hogy feladatadóivá és tisztjeiévé válljatok felettük. És ezek után vegyétek rá őket akár erőszakkal,
hogy dolgozznak, de fizettséget ne kapjanak! És ezáltal mi megerősödünk, városaink is megerősödnek
és Izráél fiai elfognak gyengülni a munka alatt. És vonjátok meg tőlük hogy a feleségeikkel legyenek,
bizonyára el fognak gyengülni ezektől is.
És mind Égyiptom vénei hallgattak a király tanácsára, és a tanácsosok és szolgák szemében jónak túnt ez és cselekedni kezdtek a király szava szerint.
És minden szolga elment a király elől, és elkezdték megvalósítani a kinyilatkoztatást egész Égyiptom
területén, Tachpancesban és Goshenben és az összes Égyiptombeli határszéli városokban is.
Mondván a következőt:
Lássátok íme, hogy mit tettek Ézsaú és Ishmael gyermekei velünk amikor pusztulásunkra jöttek.
Namármost a király megparancsolta nekünk, hogy erősítsük meg országunkat, hogy Pithom és Rameses
városokat felkészítsük és megerősítsük csatakészre, háthogyha jönnek megint.
Bárki közületek minden Égyiptom és Izráél népei akik jöttök építkezni, napidíjat kaptok a király
által, pontosan úgy ahogy ő parancsolta azt nekünk.
És amikor egész Égyiptom és minden gyermeke Izráélnek hallotta amit a Fáraó szolgái beszéltek,
jöttek Égyiptomiak és Izráél gyermekei közül valamennyien hogy építkezzenek a fáraó szolgáival,
Pithommal és Ramesessel, de egy sem jött Lévi gyermekei közül hogy segítsék felebarátaikat az
építkezésben.
És minden szolgája és hercegei a Fáraónak ravaszul jöttek eleinte építkezni Izáél gyermekeivel
együtt mint naponta felbérelt munkások, és meg is adták eleinte Izráél gyermekeinek fizettségüket.
És minden szolgája a Fáraónak Izráél gyermekeivel együtt építeztek és alkalmazva voltak ebben a 
munkában egy hónapig.
És a hónap végén, minden szolgája a Fáraónak titkon elkezdtek visszavonulni Izráél gyermekeitől.
És Izráél tette a dolgát továbbra is, és még mindig kapták fizettségüket naponta úgyahogy járt,
mert valamennyi Égyiptomi még dolgozott velük egyetembe. Igytehát az Égyiptomiak adták a bérüket
Izáélnek, abban a rendben, hogy a velük dolgozó Égyiptomiak is megkaphassák bérüket.
És egy év és négy hónap alatt minden Égyiptomi visszavonult Izráél gyermekeitől, hogy csak
Izráél gyermekei maradtak a munkára. És ezekután azok az Égyéptomiak akik visszavonultak az Izráéleiktől, vissztértek és elnyomokká és munkaadókká váltak felettük, és valamennyien felettük álltak
hogy megkapják tőlük mindazt amit tőlük kaptak hogy az ő munkájuknak legyen ez a bére.
És az Égyiptomiak tették ezt ilyen módon, hogy rendszerint megszomorítsák őket a munkájukbam.
És minden gyermeke az Izráélnek volt ily módon kezelve az ő munkájukban, és az Égyiptomiak
visszakoztak bárhogy is fizetni Izráél gyermekeinek ettől az időtől kezdve.
És ahogy valamennyi férfiak Izráélből megtagadták a munkát, hivatkozva hogy nincsenek fizetve érte,
akkor a végrehajtók (követelők) és a Fáraó szolgái elnyomták őket és rájuk sújtottak kemény ütésekkel,
és erőszakkal térítették őket vissza hogy felebarátjukkal végezzék a munkát.
Ésígy tettek az Égyiptomiak alá Izráél gyermekeinek minden napokon.
És minden gyermeke Izráélnek hatalmas félelemben volt az Égyiptomiaktól ebben a dologban, és
ők egyedül visszatértek munkájukhoz fizetés nélkül.
És Izráél gyermekei megépítették Pithomot és Ramesest, és minden gyermeke Izráélnek tette ezt,
valamennyien téglákat készítettek, valamennyien építkeztek és Izráél gyermekei építkeztek és
erősítették Égyiptom földjét és falait, és Izráél gyermekei foglalatoskodtak ezzel a munkábal
sok éven át, egészen addig míg az Úr megemlékezett rólok és ki nem hozta őket Égyiptom földjéről.
De Lévi gyermekei nem voltak alkalmazva ezzel a munkával Izráéli felebarátjukhoz hasonlóan
kezdetektől fogva az Égyiptomból való kivonulás napjátólig. Mivelhogy Lévi gyermekei tudták, hogy
az Égyiptombéliek hazugul beszéltek, hogy megtévesszék az Izráélitákat, ígytehát Lévi gyermekei,
visszakoztak megközelíteni a munkát, az ő felebarátjukkal.
És az Égyiptomiak nem fordították figyelmüket, hogy rávegyék Lévi gyermekeit a munkára ezek után,
mivel ők nem vettek részt a többi Izráéleti testvérükkel ebben kezdetektől fogva, ígytehát
az Égyiptomiak egyedül hagyták őket.
És az Égyiptomi férfiak kezei folytonos súlyosbítással irányultak Izáél gyermekeire ebben
a munkában, és az Égyéptomiak szigorral dolgoztatták Izráél gyermekeit.
És az Égyiptomiak megkeserítették Izráél gyermekeinek életét kemény munkával, mozsárral és téglával,
és mindenféle munkamódszerrel a területen.
És Izráél gyermekei Melol a királyt Meror a királynak hívták, mivel hogy megkeserítette a király
az ő életüket.
