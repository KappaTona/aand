## Resurrection Receiving Holy Spirit ; Salvation requirement

## John 20:19-22

```
19
it being
then
evening the day
that the first
[of the] week
and the doors
having been shut
where were the disciples
because of the fear
[of] the Jews
came
the
Jesus
and stood in the midst
and He says to them
peace to you

20
and this having said
He showed [His] hands
and [His] side to them
rejoiced
then the disciples 
having seen
the Lord

21
He said
then
to them
again
peace to you
as
has sent forth
Me
the
Father
I also send you

22
and this having said
He breathed on
(ΕΝΕΦΥΣΗΣΕΝ)
and He says to them
receive the Breath Holy
(ΠΝΕΥΜΑ)

```

## Romans 10:9 Receive Holy Breath

```
that if your are confessing
in the mouth of you
the Lord Jesus
and are believing
in the heart of you
that the God raised [Him]
out of the dead
you will be saved
```

## And Jesus said there is more to receive shortly before ascension

## Luke 24:48-49

```
you are witnesses
of these things
and
I [am]
sennding the promise (ΕΠΑΓΓΕΛΙΑΝ)
of the Father
of Me
upon you
you however remain
in the city until
you are clothed with [power]
out of on high
```

## Acts 1:4-5, 8

```
4
and being assembled
He instructed them
from Jerusalem
not to depart
but to await
the promise (ΕΠΑΓΓΕΛΙΑΝ) {same word as above}
of the Father
that [which] you heard
of Me

5
that John indeed
baptized in water
you
however in Holy Spirit
(ΠΝΕΥΜΑΤΙ)
will be baptized
after not many these days

8
but you will reveive power
having come
the Holy Spirit
(ΠΝΕΥΜΑΤΟΣ)
upon you and
you will be for Me
witnesses in
Jerusalem and in all
Judea
and
Samaria
and to the uttermost part
of the earth
```


## Pentecost receiving

## Acts 2 Fulfillment of the Promise
## Acts 2:1-4

- Immersed
- Filled
- Overflow

`Matthew 12:34 second part: for the mouth speaks out of that which fills the heart (33 speaks about for the tree is known by fruits)`


```
1
and in the arriving
[of] the day [of] Pentecost
they were all together
at the same [place]

2
and came suddenly out of
heaven a sound like [the] rushing
[of a] wind (ΠΝΟΗΣ)
violent
and it filled (ΕΠΛΗΡΩΣΕΝ)
all the house where they were sitting

3
and there appeared to them
dividing tongues (ΓΛΩΣΣΑΙ)
as [of] fire and sat upon
one each
of them

4
and
they were filled (ΕΠΛΗΣΘΗΣΑΝ)
all
Spirit (ΠΝΕΥΜΑΤΟΣ)
Holy
and began to speak
[in] other tongues (ΓΛΩΣΣΑΙΣ)
as the Spirit (ΠΝΕΥΜΑ)
was giving [to them]
to speak out
```

## Peter by receiving More insight to scripture quoting Joel 2:28-29

## Acts 2:14-18 (Acts 2 whole chapter if you will it)

```
having stood up however
the
Peter
with the eleven
he lifted up the voice
of him and spoke forth
to them
men
Jews
and the [ones]
inhabiting
Jerusalem
all this to you known
[let] be
and give heed to
the words of me

15
not for as you
suppose
these are drunk
it is
for [the]
hour
third [of] the day

16
but this is [that]
spoken through the prophet Joel

17
and it will be
in the last days says
the
God
I will pour from the
Spirit (ΠΝΕΥΜΑΤΟΣ)
of Me
upon all flesh and
will prophesy
the sons of you
and the daughters
of you
and the young men of you
visions will see
and the elders of you
dreams will dream

18
and both upon the slaves of Me
and upon the handmaidens of Me
in the days those
I will pour out from
the Spirit (ΠΝΕΥΜΑΤΟΣ) of Me
and they will prophesy (ΠΡΟΦΗΤΕΥΣΟΥΣΙΝ)

```


## Ephesians 1:13-14

```
in whom also you
having heard
the Word [of] Truth the Gospel
[of] [your] salvation
in whom also
having believed
you were sealed
[with] the
[Holy] Spirit (ΠΝΕΥΜΑΤΙ)
[of] promise (ΕΠΑΓΓΕΛΙΑΣ)

14
who is [the] pledge
[of] the inheritance
of us unto [the] redemption
[of] the acquired possession unto
[the] praise [of] the glory of Him
```

## Now then

Now then. Every person received the Holy Spirit has salvation.
But what to do next. Obeying the Commandments is True. Receiving the Power of the Holy Spirit is True. Both is an ongoing experience.

After Acts 2, every time `'Receive the Holy Breath'` mentioned, it is referring to the Pentecost fulfilled Promise.

## Acts 8:12 the Samarithans are saved and have salvation at this point but the power of the Holy Spirit is not yet upon them.

```
when however they believed
Philip proclaiming the Gospel
concerning the Kingdom [of] God
and the Name of Jesus [the] Christ (ΧΡΙΣΤΟΥ)
they were baptized men both and women
```

## Acts 14-17 now they Have.

```
having heard
and
the [ones] in Jerusalem
[the] apostles
that 
had received
Samaria the Word [of] God
they sent
to them
Peter and John

15
who having come down
prayed concerning them
to receive
[the] Spirit (ΠΝΕΥΜΑ) Holy

16
not yet
for
He was
upon any of them fallen
only
and
baptized
they had been
into
the Name
[of] the
Lord (ΚΥΡΙΟΥ)
Jesus

17
then they began laying
the hands upon them
and the received
[the] Spirit (ΠΝΕΥΜΑ) Holy

```

## Acts 10:47 Peter confirms the visible manifested power of the Holy Breath because the listeners: immersed (Cornelius prayed, donated to the Jewish people) filled (Obeyed the command [which is a Command from God confirmed by Acts 10:19-20] of the Angel to call for Peter), and overflow (speaking all sorts of mysteries this is different from the multitude, where everyone heard their own tongue; 1 Corinthians 12:3-11).

```
46

they were hearing
for
them
speaking
[in] tongues (ΓΛΩΣΣΑΙΣ)
and magnifying
the
God
then answered
Peter

47
If not
water is able
to withold
anyone
not to baptize these
who the
Spirit (ΠΝΕΥΜΑ)
Holy
have received as also
we [received]
```

## parallel to John 7:37-39

as of: water for baptism

as of: rivers of water outflow

```
If not
water is able
to withold
```

## Acts 19:1-6

```
it came to pass
and
in the [time]
Apollos was in Corinth
Paul having passed through
the upper parts to come
to Ephesus
and
having found certain disciples

2
he said also to them
if [the] Spirit (ΠΝΕΥΜΑ) Holy
did you receive having believed
[and they said]
to him but not even that
[there is] a Spirit (ΠΝΕΥΜΑ) Holy
did we hear

3
he said
then into what
then were you baptized
and
they said into [of] John [the] baptism

4
said
and
Paul
John baptized a baptism [of] repentance
[telling] the people
in the [One] coming
after him so that
they should believe
that is in Jesus

5
having heard
and
they were baptized
in the Name [of]
the Lord (ΚΥΡΙΟΥ)
Jesus

6
and
having laid on them
the Paul
hands
of him
the
Spirit (ΠΝΕΥΜΑ)
Holy
[came] upon them
[and] they were speaking
in
tongues (ΓΛΩΣΣΑΙΣ)
and prophesying (ΕΠΡΟΦΗΤΕΥΟΝ)
```

## support verse: 1 Corinthians 14:4, 13, 22, 37-39

```
4
the
one
speaking [in] tongue (ΓΛΩΣΣΗΙ)
himself edifies
the
and
prophesying [the] church edifies

13
therefore
the one
speaking
[in] a tongue (ΓΛΩΣΣΗΙ)
let him pray to
interpret

22
so
tongues (ΓΛΩΣΣΑΙ)
[are] unto a sign (ΣΗΜΕΙΟΝ)
not [to] the [ones]
believing but [to] the unbelieving
however
prophecy (ΠΡΟΦΗΤΕΙΑ)
[is] not
[to] the
unbelieving
but [to] the believing

37
if anyone thinks a prophet himself to be
or spiritual (ΠΝΕΥΜΑΤΙΚΟΣ)
let him recognize
what I write to you
that
[of the] Lord (ΚΥΡΙΟΥ)
are
[the] commands

38
if
and
anyone
is ignorant [of this]
let him be ignored

39
so brothers be zealous [to]
prophesy (ΠΡΟΦΗΤΕΥΕΙΝ)
but
do not forbid
[to speak in]
tongues (ΓΛΩΣΣΑΙΣ)
```

+++++

## Luke 11:11-13

```
which
and
out of
you
[who is]
a father
may ask the son a fish
and in place of a fish
a serpent to him
will he give

12
or also
may he ask
an egg
will he give to him
a scorpion

13
if therefore
you evil being know
[how] [good]
gifts to give [to] the children
of you
how much more the Father
the out of heaven will give
[the] Spirit (ΠΝΕΥΜΑ)
Holy
[to] the [ones] asking Him
```

## John 7:37-39

```
in
and
the last day
the great [day]
[of] the Feast (ΕΟΡΤΗΣ)
stood
the
Jesus
and
cried out
saying
if anyone thirsts let him come
to Me
and drink

38
the [one]
believing in Me
as has said
the Scripture (ΓΡΑΦΗ)
rivers out of the belly of him
will flow [living] water

39
this
and
He said
concerning the
Spirit (ΠΝΕΥΜΑΤΟΣ)
whom were about to receive
the [ones]
having believed
in Him
not yet [given]
for
was
[the] Spirit (ΠΝΕΥΜΑ)
because
Jesus
not yet
was glorified
```

ΕΟΡΤΗΣ: feast, festival, (Aram GRK: Pascha the Passover John 13:1)

## Acts 2:38-39

```
Pater
and
to them
repent (ΜΕΤΑΝΟΗΣΑΤΕ)
he declared
and
be baptized
every one of you
in the Name [of]
Jesus [the] Christ (ΧΡΙΣΤΟΥ)
unto
forgivness
[of] the
sins
of you
and
you will receive
the gift (ΔΩΡΕΑΝ)
[of] the
Holy
Spirit (ΠΝΕΥΜΑΤΟΣ)

39
to you
for
is the promise (ΕΠΑΓΓΕΛΙΑ)
and
[to] the children of you
and [to] all
the [ones] at a distance
shall call to Himself
the Lord (ΚΥΡΙΟΣ)
the God
of us
```

Character requirements:
- Be thirsty
- Come to Jesus, He is the Baptizer
- Drink. No body can force you to drink. Decision of your own will.
- Open your mouth. No one can speak with closed mouth.
Supernatural infilling. Supernatural outflow.
- How do you know it is the right thing. Luke 11:11-13 You asked for the right thing. You went to Jesus. You drank.
- The Accuser will come in and say: this is not the right thing. You making it up. Remind yourself: your are speaking but the Holy Breath fuels it, gives you the Word to speak.
