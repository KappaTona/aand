### part 3 source https://www.youtube.com/watch?v=Bf4gbKRu23o&list=PLVAgfik35fd-U0h8XtVfspuAgHOoZpagW&index=3

## support verses

proverbs 2:1-5
```
1 My son, if you will receive my words
And treasure my commandments within you,
2 

So that your ear is attentive to [skillful and godly] wisdom,
And apply your heart to understanding [seeking it conscientiously and striving for it eagerly];
3 

Yes, if you cry out for insight,
And lift up your voice for understanding;
4 

If you seek skillful and godly wisdom as you would silver
And search for her as you would hidden treasures;
5 

Then you will understand the [reverent] fear of the Lord [that is, worshiping Him and regarding Him as truly awesome]
And discover the knowledge of God.
```

## Intro

- written date before the destruction of the tempke AD 70.
- distinctive revelation: Jesus as High Priest
- strong encouaragement: stirr up , cheer up
- high priest
- promise oath covenant all three depends on the high priest
- offering sacrafice and blood: required to estabilish the covenant
- faith hope confession: required from us as our response to receive what is available in the covenant
- inheritence, rest, perfection: objectives which we should be moving
- warnings: neglect, unbelief, apostasy, willfully continue to sin, coming short of the grace of God
- main theme revelation of heavenly realities (John 3:12) therefore only by revelation it is revealed
- inferior and superior comparison:
    - Angels and Jesus
    - Moses and Jesus
    - Levitical priesthood Melchizedek priesthood
    - old covenant and the new
    - tabernacle of Moses and heavenly tabernacle
    - Levitical sacrafices and Jesus sacrafice
    - mount Sinai and mount Zion
- let us: decision and cooperate decision (not you alone, fellow believers)
 
## 1:3 notes

- `exact represntation`: Greek word: sealring (inprint in the wax)
- `substanance`: that which is under something else and it keeps it up (nature is a close translation but not enough)
- `carries all`: upholds
- GodHead parable of the Sun:
    - substanace of the Sun (Father)
    - visible radience is the Son of God Jesus
    - the rays that bring to sun to our eyes is the Holy Spirit
    no rays = no revelation
- Collosians 1:13-14; 1:15-18


## 1:4

- Ephesians 1:21
- Filippians 2:9-11
    - gave and inherited, God highly exalted Him, condition met, reward


