Original HUN below


## Heavenly Father Glory to You in You For You. 


```
(All Bible verses I mention are from KAR (Hungarian) and I compare the AMP versions of the verses.)

 the quote is from the KAR (archaic Hungarian translation). And the note: is One Word.
"ÉS". (means: "AND") and it references the "and" connection between: "josagod" and "kegyelmed".

The Bible Kar uses "And" all the time. So it could sound "boring" who is weak in the spirit because the sleepiness sneaks in and the one bimbambum founds the one in dreaming already 
AMP: 9 Ands
KAR: 4 és
And they italic so it could mean that the origin version does not even contain the word: "and"
```

+++++
 
## The words:

```
"Jòsàgod": means Jò (Good) Jòsàg (something that originated from Good). (Jòsàg)od (od is the Hungarian possession affix ) in this very same psalm my book titles it: "A Good Shepherd" "My Lord is my shepherd and I am not experiencing any lack in supplies" (szükölködöm) Jesus Himself say: I AM the Good Shepherd, no one is Good but God.
"Kegyelmed": (Infinite Forgiveness [{I say like this, but the verses below represents it} because I am unable to merit the grace and unable to count the mercy or even know which word means what. English is not my first language, it is Hungarian the tongue (language) I have]) this part is pointed what inside the content of my notes "Es Hogy" and "itt".
(Es hogy: And How. Further personal notes. Which I can show you if you decide that you need to see it)
(Itt: Here.)
```

```
I will try my best to translate them to English since all of them in Hungarian.
But before that: I can share the Bible references that are in the "itt" notes.
It is the prophetic vision that I wanted to share and mentioned in the self-promo section,
If at least one person asks it. I will share it. You are the one person. Who asked.
```

+++++

## Therefore the testimony of the Word of God:

```
ÉS
Bizonyságtétel:  (attestation)
Tétel: (assertion)
Jakab (James) 4:15  
Bizonyság: (witness)
Dàniél 1:9, 11-12 (Book of Daniel ch1 v9 v11-12)
Dániel 2: 18-19 (Book of Daniel ch2 v18-19)
Jakab 1:5-6 (James ch1 v5-6)
Példabeszédek 2:6-7  (Proverbs ch 2 v6-7)
Zsoltárok 3:4 (Psalm ch3 v4)
Minta-Példa: (Pattern-Example)
 Máté 7: 7-8 (Matthew ch7 v7-8)
```

+++++

## This happened around: 2021 11th month just before the end of the year I had the vision. It got fulfilled at 2022. 02nd month 14th day.

Személyes tapasztalat:
(Personal Pattern-Example)
- És mondám: bizonyára ha ráfekszek erre a témára, hát lelni fogok. Így lett.
    - And I said: Surely if I truly tryhard to understand the topic, I will find. And it was so. 
- És mondám: Uram ha te is úgy akarod és én élek: Akkor adj olyan munkát amit otthonról és elégedetten tudok végezni. Így lett.
    - And I said: My Lord if you will it so and I live: Then give me such workplace: which I can do from home and be satisfied and can finish on time. It was so.
- És mondám: Böjtölni fogok és munkát fogok keresni pont olyat amilyet az Úr szán nekem. Így lett.
    - And I said: I will fast and I will seek for a job exactly what the Lord intend for (to) me. It was so.
- És látám: Ülök az egyetemi padsorban és rengeteg papír vala előttem, és választanom kell egyet -- melyen ismerős nyelven írások valának és kőből van eme irat -- És keresem a munkát és idő után lelék. Ahol a bemutatkozó interjú legvégén kapék egy naplót ami egy STONE BOOK.
    - And Behold (in my vision): I sit at the university at the students place and a whole lot of document infrontof me and I had to chose one -- such on: familiar language (but I could not translate) on the one writings being upon: written words. And this document is from stone -- And I search for the job and 2 months later I found. Where and When the first personal interview (after we both party agreed and accept) happened we were signing the documents. Where after I receive a datebook with unfilled dates (dates left blank). And its name: STONE BOOK (because the papers inside were created by pressed stones, thus being water proof. And heavy fire resistant) and a note in itself said: I AM UNIQUE for you (I have picture of this if you decide you need to see it) and it detailed how come that this book pages are from stone.)

- És végzem a munkám, hát úgyahogy. Hát dicsőséget szerezve az Isten nevének.!
    - And I did my job, Glory to God in His Name {the word here "ugyahogy" is equal to the notes I have in: "Es Hogy" and "itt"}. (Lit: Gaining [working towards to produce fruits] Glory to the God Name)
    - És íme ez többek között egy példa,: az Isten szeretete rám nézve úgyahogy.
- And behold this is amongst many, one: Példa what is God Love (being look at me) unto me úgyahogy.

## ÉS továbbá vegyük észre

```
(AND continuously AND furthermore observe AND purchase it into our mind)
Zsidòk 4:16 (Hebrews ch4 v16)
(Kegyelem, Irgalmassàg, Kegyelem)

Jànos 16:23-24, 27
(John ch16 v23-24, v27)

The multiple "And" here reminds me to have Hope and Pray day by day
```

+++++++++


## Word Index:

```
Tétel:
Érvényességet kifejező állítás,
amely egy viszony, tény,
igaznak tekintett megállapítás fennállását jelzi.
Erre további állítások, illetve igazságok épülnek.

(A statement expressing validity,
which indicates the existence of
a relationship,
fact, or
statement considered true.
Further statements and truths are based on this.)
```

```
Bizonysàg: 
1.) Igazsàg (unfalse-able truth, axiom)
2.) tanúsàg (action: bearing a witness about [literally: putting truth about by])
3.) Tanùsàg (person: bearing confession and putting axiom in front of others as proof)
4.) Bizonyíték: evidence.
```

```
Minta-Példa: (Pattern-Example
it makes more sense in Hungarian because it is one word.
And used instead of "example". When a teacher says:
This is the example we do in class.
"Ez az a mintapélda amit tanulunk az óràn.")
```

```
Lélek: (Spirit, Soul, Life, Breath in Hungarian)
Léle(k)gzet
is an amazing and wonderful word.
Many times used in archaic Hungarian poetry.
Yet in elementary school a lot struggle to type/write it down.
Because it sounds like "Lélekzet" but it is written with "g".
Because the letters 'k' and 'g' is so similar.
Yet the meaning in context does not change but with affix
it does not make sense.
Példa:
Lék szomj - edge, whole thirst
Lég szomj - breath thirst
Lélek szomj - Soul thirst
Léleg(zet) szomj - Breath thirst (zet is affix)
```


```
Példabeszédek: 
is formed from two Hungarian words:
 Példa (as an example, pattern what to do,
 example behavior set that one person act out continuously)
Beszédek: Tales, speech, words, content;
expressed explicitly by vocal sound. (Plural form)
```

```
And: in Judgement Logic (
https://en.m.wikipedia.org/wiki/Judgment_(mathematical_logic) ); Tautology and Logic and Machine Pattern Recognition.
it is stated that every statement which has actor and action can be formed until the predicates in forms of evaluates only to an "and". using the Judgement Logic True/False tables the statement can be formed until only contains "and" connector. Thus forming the statement to absolute true. And can be false based on/in/out parameters (Similar to computer science: and logic gate in transistors )
```

+++++
 
## Quoting from the "Es hogy" (which I can show you if you decide you need to see it) notes:

```
"Ök általa Szeretetben viselem az ajándék-felszerel ÉSt."
(let us through God in Love equip the gift equipment And).
Now this is Hungarian wording. 
Ök àltala: multiple person acting out as one (similar to the genesis let us)
Ajndék: gift
Felszerel: to equip
FelszerelÉS: equipment ( here the "ÉS" is a noun affix.)
```

## Quoting from the "itt" notes

- Daniel 1:9 méltò = kegyelem és irgalmassàg
- Psalm 3:4 (cross: Genesis 15:1 the Shield of Faith, Psalm 27:6, Psalm 110:7)


## Now then,  I went through briefly the testimony. I share Isaiah 63:8 (the picture if you decide that you need to see it) 

```
And Isaiah 63:7 (Jòsàg, Irgalom, Kegyelem) which has the Same "And" that in Psalm 23:6
The same "And" as an axiom in me that makes my brain to act on How.
Without reasoning the Why. Because why is the many ways that can happen.
And How is the only and one time how it happens and finished.
For me From Father Through His Son in His Lélek. Amen and Halelujah.
```


++++++++++++
+ Original +
+ Original +
++++++++++++

ÉS
Bizonyságtétel:
Tétel: Jakab 4:15
Bizonyság:
Dániel 1:9, 11-12
Dániel 2: 18-19
Jakab 1:5-6 Példabeszédek 2:6-7 Zsoltárok 3:4
Minta-Példa: Máté 7: 7-8
---
ÉS
Bizonyságtétel:
Tétel:
Holott ezt kellene mondanotok: Ha az ÚR akarja és élni fogunk, akkor ezt vagy azt fogjuk tenni.
Bizonyság:
- És az Isten kegyelemre és irgalomra (tudni illik: méltóvá) tevé Dánielt az udvarmestereknek fejedelme előtt;
- És mondá Dániel a felügyelőnek, a kire az udvarmesterek fejedelme bízta vala Dánielt, Ananiást, Misáelt és Azariást: Tégy próbát, kérlek, a te szolgáiddal tíz napig, és adjanak nékünk zöldségetféléket, hogy azt együnk és vizet, hogy azt igyunk.
- És szóla velök a király, és mindnyájok között sem találtaték olyan, mint Dániel, Ananiás, Misáel és Azariás, és állának a király előtt. És minden bölcs és értelmes dologban, a mely felől a király tőlök tudakozék, tízszerte okosabbaknak találá őket mindazoknál az írástudoknál és varázslóknál, a kik egész országban valának. És ott vala Dániel a Cyrus király első eszendejéig.
- Hogy kérjenek az egek Istenétől irgalmasságot e titkok végett, hogy el ne veszszenek Dániel és az ő társai a többi babiloni bölcsekkel együtt. Akkor Dánielnek megjelenteték az a titok éjjeli látásban. Áldá akkor Dániel az egek Istenét.
<==
- Ha pedig valakinek közületek nincsen bölcsessége, kérje Istentől, a ki mindenkinek készségesen és szemrehányás nélkül adja; és megadatik néki. ÉS kérje hittel, semmit sem kételkedvén: mert a ki kételkedik, hasonlatos a tenger habjához, a melyet a szél hajt és ide s tova hány.
- Mert az Úr ád bölcsességet, az ő szájából tudomány és értelem származik. Az igazaknak valóságos jót rejteget, paizst a tökéletesen járóknak. (tudni illik: a Hit Paizsa és még inkább oltalmat: Hogy Mozes kérte hogy láthassa a Dicsőség Királyát, akit még élő ember nem láttot és élt, ugyanez az oltalom ez a paizs)
- De te, oh Uram! paizsom vagy nékem, dicsőségem, az, a ki felmagasztalja az én fejemet.
.:. (Therefore)
- Kérjetek s adnak majd nektek, keressetek és találni fogtok, zörgessetek és megnyitnak majd nektek. Mert minden kérő kap, minden kereső talál, és minden zögetőnek megnyitnak majd.
.......
Személyes tapasztalat:
- És mondám: bizonyára ha ráfekszek erre a témára, hát találni fogok. Így lett.
- És mondám: Uram ha te is úgy akarod és én élek: Akkor adj olyan munkát amit otthonról és elégedetten tudok végezni. Így lett.
- És mondám: Böjtölni fogok és munkát fogok keresni pont olyat amilyet az Úr szán nekem. Így lett.
- És látám: Ülök az egyetemi padsorban és rengeteg papír vala előttem, és választanom kell egyet -- melyen ismerős nyelven írások valának és kőből van eme irat -- És keresem a munkát és idő után találék. Ahol a bemutatkozó interjú legvégén kapék egy naplót ami egy STONE BOOK.
- És végzem a munkám, hát úgyahogy. Hát dicsőséget szerezve az Isten nevének.!
- És íme ez többek között egy példa,: az Isten szeretete rám nézve úgyahogy.

