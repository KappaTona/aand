new birth: John's Gospel: 1; 11-13. 3; 1-8
speaking the unknown tongue: Acts: 2; 1-18. 10; 44-47. 19; 1-6
Luke: 24; 49. Acts: 1; 5, 8.

---

**John 1; 11-13**

11 He (Jesus) came to that which was His own [that which belonged to Him—His world, His creation, His possession], and those who were His own [people—the Jewish nation] did not receive _and_ welcome Him.

12 But to as many as did receive _and_ welcome Him, He gave the right [the authority, the privilege] to become children of God, _that is_, to those who believe in (adhere to, trust in, and rely on) His name—

13 who were born, not of blood [natural conception], nor of the will of the flesh [physical impulse], nor of the will of man [that of a natural father], but of God [that is, a divine and supernatural birth—they are born of God—spiritually transformed, renewed, sanctified].


**John 3; 1-8.**

1 Now there was a certain man among the Pharisees named Nicodemus, a ruler (member of the Sanhedrin) among the Jews,
2 who came to Jesus at night and said to Him, “Rabbi (Teacher), we know [without any doubt] that You have come from God as a teacher; for no one can do these signs [these wonders, these attesting miracles] that You do unless God is with him.”

3 Jesus answered him, “I assure you _and_ most solemnly say to you, unless a person is born again [reborn from above—spiritually transformed, renewed, sanctified], he cannot [ever] see and experience the kingdom of God.”

4 Nicodemus said to Him, “How can a man be born when he is old? He cannot enter his mother’s womb a second time and be born, can he?”

5 Jesus answered, “I assure you _and_ most solemnly say to you, unless one is born of water and the Spirit he cannot [ever] enter the kingdom of God.

6 That which is born of the flesh is flesh [the physical is merely physical], and that which is born of the Spirit is spirit.

7 Do not be surprised that I have told you, ‘You must be born again [reborn from above—spiritually transformed, renewed, sanctified].’ 

8 The wind blows where it wishes and you hear its sound, but you do not know where it is coming from and where it is going; so it is with everyone who is born of the Spirit.”


**Acts 2; 1-18**

1 When the day of ![a](null "Pentecost Feast of Weeks was observed at the time of the grain harvest and the offering of the first fruits, and was one of the three great annual Jewish festivals, along with Passover and the Feast of Tabernacles Booths. Just as Jesus’ sacrifice was the fulfillment of Passover, the coming of the Holy Spirit was the fulfillment of Pentecost.") Pentecost had come, they were all together in one place,

2 and suddenly a sound came from heaven like a rushing violent wind, and it filled the whole house where they were sitting.

3 There appeared to them tongues resembling fire, which were being distributed [among them], and they rested on each one of them [as each person received the Holy Spirit].

4 And they were all filled [that is, diffused throughout their being] with the Holy Spirit and began to speak in other ![b](null "γλώσσες Γλώσσες") tongues (different languages), as the Spirit was giving them the ability to speak out [clearly and appropriately].

5 Now there were Jews living in Jerusalem, devout _and_ God-fearing men from every nation under heaven.

6 And when this sound was heard, a crowd gathered, and they were bewildered because each one was hearing those in the upper room speaking in his own language or dialect.

7 They were completely astonished, saying, “Look! Are not all of these who are speaking Galileans?

8 Then how is it that each of us hears in our own language _or_ native dialect?

9 [Among us there are] Parthians, Medes and Elamites, and people of Mesopotamia, Judea and Cappadocia, Pontus and ![c](null "Modern Turkey") Asia [Minor],

10 Phrygia and Pamphylia, Egypt and the districts of Libya around Cyrene, and the visitors from Rome, both Jews and proselytes (Gentile converts to Judaism),

11 Cretans and Arabs—we all hear them speaking in our [native] tongues about the mighty works of God!”

12 And they were beside themselves with amazement and were greatly perplexed, saying one to another, “What could this mean?”

13 But others were laughing and joking and ridiculing them, saying, “They are full of sweet wine and are drunk!”

**Peter’s Sermon**

14 But Peter, standing with the eleven, raised his voice and addressed them: “Men of Judea and all you who live in Jerusalem, let this be explained to you; listen closely _and_ pay attention to what I have to say.

15 These people are not drunk, as you assume, since it is [only] the third hour of the day (9:00 a.m.);

16 but this is [the beginning of] what was spoken of through the prophet Joel:

17 

    ‘AND IT SHALL BE IN THE LAST DAYS,’ SAYS God,

    ‘THAT I WILL POUR OUT MY SPIRIT UPON ALL MANKIND;

    AND YOUR SONS AND YOUR DAUGHTERS SHALL PROPHESY,

    AND YOUR YOUNG MEN SHALL SEE [divinely prompted] VISIONS,

    AND YOUR OLD MEN SHALL DREAM [divinely prompted] DREAMS;

18 

    EVEN ON MY BOND-SERVANTS, BOTH MEN AND WOMEN,

    I WILL IN THOSE DAYS POUR OUT MY SPIRIT

    And they shall prophesy.


**Acts 10; 44-47**

44 While Peter was still speaking these words, the Holy Spirit fell on all those who were listening to the message [confirming God’s acceptance of Gentiles].

45 All the circumcised believers who came with Peter were amazed, because the gift of the Holy Spirit had been poured out even on the Gentiles.

46 For they heard them talking in [unknown] tongues (languages) and exalting _and_ magnifying _and_ praising God. Then Peter said,

47 “Can anyone refuse water for these people to be baptized, since they have received the Holy Spirit just as we did?”

**Acts 19; 1-6**

1 It happened that while Apollos was in Corinth, Paul went through the upper [inland] districts and came down to Ephesus, and found some disciples.

2 He asked them, “Did you receive the Holy Spirit when you believed [in Jesus as the Christ]?” And they said, “No, we have not even heard that there is a Holy Spirit.”

3 And he asked, “Into what then were you baptized?” They said, “Into John’s baptism.”

4 Paul said, “John performed a baptism of repentance, _continually_ telling the people to believe in Him who was coming after him, that is, [to confidently accept and joyfully believe] in Jesus [the Messiah and Savior].”

5 After hearing this, they were baptized [again, this time] in the name of the Lord Jesus.

6 And when Paul laid his hands on them, the Holy Spirit came on them, and they began speaking in [unknown] tongues (languages) and prophesying. 

**Luke 24; 49**

49 Listen carefully: I am sending the Promise of My Father [the Holy Spirit] upon you; but you are to remain in the city [of Jerusalem] until you are clothed (fully equipped) with power from on high.”

**Acts 1; 5 and 8**

5 For John baptized with water, but you will be baptized _and_ empowered _and_ united with the Holy Spirit, not long from now.”

8  But you will receive power _and_ ability when the Holy Spirit comes upon you; and you will be My witnesses [to tell people about Me] both in Jerusalem and in all Judea, and Samaria, and even to the ends of the earth.”

In Jesus (the Christ, Anointed) name Amen!
