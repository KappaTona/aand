https://www.livinggreeknt.org/findings/I-AM.php

37 Hebrew word: ואהיה

AND I AM = I WILL BE

1. Genesis 26:3

```
sojourn in [this] land
AND I AM
with you
And I have blessed you
because to you
and to your Seed
I will give
(aleftav)
all [these] lands
and I will establish
(aleftav)
the oath that 
I swore to Abraham 
your father.
```

2. Genesis 31:3

```
and said the LORD to Jacob
return to the land of your
fathers
and to your birthplace
AND I AM
with you
```

3. Exodus 3:12

```
and He said that
I WILL BE
with you and this for you
is the sign that I am
sending you when you have taken out
(aleftav)
the people out of Egypt you will serve
(aleftav)
God on this mountain
```

4. Exodus 3:14
5. Exodus 3:14
6. Exodus 3:14

```
and said God to Moses
I WILL BE
WHO
I WILL BE
and He said thus you will say
to the sons of Israel
I WILL be
has sent me
to you
```

7. Exodus 4:12 

```
and now go
and I Myself
I WILL BE
with your mouth
and I will direct you
that which you will speak
```

8. Exodus 4:15

```
and you will speak to him
and will place
(aleftav)
the words in his mouth
and I Myself
I WILL BE
with your mouth and with his mouth
and I will direct you
(aleftav)
that which you will do
```

9. Deuteronomy 31:23

```
and He commissioned
(aleftav)
Joshua
son of Nun and He said
be strong and be courageous
because you will bring
(aleftav)
the sons of Israel into the land
that i promised to them
and I Myself
I WILL BE
with you
```

10. Joshua 1:5

```
not will stand a man before you
all the days of your life
just as I was with Moses
I WILL BE
with you
not
I will leave you
and not
I will forsake you
```

11. Joshua 3:7

```
and said the LORD to Joshua
this day I will being making
you great in the eyes of all
Israel so that they will know
that just as I was with Moses
I WILL BE
with you
```

12. Judges 6:16

```
and said to him the LORD
because
I WILL BE
with you and you smite
(aleftav)
Midian as one man
```

13. 2 Samuel 7:6

```
beacuse not I have dwelt in a house
from the day I took up
(aleftav)
the sons of Israel out of Egypt and up to
this day
AND I AM
moving in a tent
and in a tabernacle
```

14. 2 Samuel 7:9

```
AND I AM
with you in every that
you went and I cut off
(aleftav)
all your enemies before you
and I will make for you a
great name
like the name of the great
who are on the earth
```

15. 2 Samuel 7:14

```
I
I WILL BE
to Him the Father
and He will be to Me the Son
when he commits
iniquity and I will chasten him
by a rod of men and by strokes
of the sons of men
```

16. 1 Chronicles 17:5

```
because I have not dwelt
in a house from the day that
I took up
(aleftav)
Israel until this day
AND I AM
moving from tent to tent
and from the tabernacle
```

17. 1 Chronicles 17:8

```
AND I AM
with you everywhere
that you go and I cut off
(aleftav)
all your enemies before you
and I will make for you a name
like a name of the great men
who are on the earth
```

18. 1 Chronicles 17:13

```
I
I WILL BE
to Him the Father
and He will be to Me the Son
and My kindness not
I will remove from His people
just as I removed from him who
was before you
```

19. 1 Chronicles 28:6

```
and He said to me
Solomon your son
he will
build My house and My courts
because I have chosen him
for My son and I
I WILL BE
to him the Father
```

20. Psalms 50:21

```
these you have done
and I was silent
you imagined
I WILL BE
like you
I will reprove you
and set in order
before your eyes
```

21. Isaiah 3:7

```
he will lift up on that day
saying not
I WILL BE
he who binds wounds and in my house
there is no bread and no cloak
and not
you will appoint me ruler
over the people
```

22. Jeremiah 11:4

```
that I commanded
(aleftav)
your fathers on the day I took them out
out of the land of Egypt
out of the furnance of iron
saying obey
My voice
and do them
according to all that
I will command you
and you will be
My people and I
I WILL BE
your God
```

23. Jeremiah 24:7

```
And I will give to them
a heart to know Me
that I AM the LORD
and they will be Mine
for a people
and I
I WILL BE
for them the God because
they will return to 
Me
with all their heart
```

24. Jeremiah 30:22

```
and you will be
to Me
for a people
and I
I WILL BE
for you the God
```

25. Jeremiah 31:1

```
at time that
declares the LORD
I WILL BE
the God
of all the families
of Israel and they
will be
for Me
the people
```

26. Jeremiah 32:38

```
and they will be
for Me
the people
and I Myself
I WILL BE
for them
the God
```

27. Ezekiel 11:20

```
in order that in My statutes
they will walk and My judgements
they will keep
and will do them
and they will be
to Me
the people
and I
I WILL BE
for them
the God
```

28. Ezekiel 14:11

```
In order that
not
will wander anymore
the house of Israel
from following Me
and not
they will profane themselves
anymore by all their transgressions
and they will be
for Me
the people
and I
I WILL BE
for them the God
declares
My Lord the LORD
```

29. Ezekiel 34:24

```
and I
the LORD
I WILL BE
for them
the God
and My servant
David a prince 
in their midst
I
the LORD
have spoken
```

30. Ezekiel 36:28

```
and you will dwell
in the land that
I gave
to your fathers
and you will be
for Me
the people
and I
I WILL BE
for you
the God
```

31. Ezekiel 37:23

```
and not
they will defile themselves
anymore by their idols
and by their detestabe things
and by all their transgressions
and I will save them out of all
their dwelling places that
they sinned in them
and I will cleanse them
and they will be
for Me
the people
and I
I WILL BE
for them
the God
```

32. Hosea 1:9

```
and He said
call his name
Lo Ammi
because you
are not
My people
and as
for Me
not
I WILL BE
your God
```

33. Hosea 11:4

```
with cords
of a man
I will draw them
with bonds
of love
AND I AM
to them
as those who raise
the yoke from
their jaws
and I will bend down
to him
I will provide
```

34. Hosea 14:5

```
I WILL BE
like dew
to Israel
he will sprout
like a lily
and strike
his roots
like Lebanon
```

35. Zechariah 2:5
36. Zechariah 2:5

```
and I Myself
I WILL BE
{for her}
declares
the LORD
a wall of fire
for her
all around
and the glory
I WILL BE
in her midst
```


37. Zechariah 8:8

```
and I will bring
them
and they will dwell
in the midst of Jerusalem
and they will be
for Me
the people
and I Myself
I WILL BE
for them
the God
in truth
and in righteousness
```


---

37 Greek word: ΕΓΩ ΕΙΜΙ

I AM

Jesus spoke: I AM

1. Matthew 14:27 Jesus walking on water.

```
straigthway
however
spoke
(the)
Jesus
to them
saying
have courage
I AM
do not fear
```

2. Matthew 22:32 Jesus quotes Exodus 3:6.

```
I AM
the
God
of Abraham
and
the
God
of Isaac
and
the
God
of Jacob
(the from)
He is not
the
God
of the dead
but 
of the living
```

3. Mark 6:50 Jesus walks on water.

```
because all saw Him
and were troubled
however straightway
He spoke with them
and
He says to them
take courage
I AM
do not fear
```

4. Mark 14:62 "Are you the Christ?" I AM.

```
(the)
and Jesus said
I AM
and you will see
the Son of Man
at the right hand
sitting of Power
and coming with
the clouds of heaven
```

5.  Luke 22:70 "Are you the Son of God?" I AM.

```
and they all said
are You the the Son of God
(the)
and to them
He was saying
you say that
I AM
```

6. Luke 24:39 Resurrected Lord to discliples: " See My ahnds and feet". I AM.

```
see the hands
of Me
and the feet
of Me
that
I AM HE
touch Me
and see that
a spirit
flesh and bones not has
as Me you see having

(A szellemnek nincs husa es csontja
mint ahogy látjátok nekem van)
```

7. John 4:26 Woman of Samaria: "Messiah comes". Jesus: I AM.

```
says to her
the Jesus
I AM
the One
speaking to you
```

8. John 6:20 Jesus walks on water. Jesus: "I AM. Do not be afraid.".

```
(the)
and
He says
to them
I AM
do not fear
```

9. John 6:35 I AM the Bread of Life.

```
to them said
the Jesus
I AM
the Bread of Life
the one coming
to Me
no
not be at
hunger
and the one
believing
in Me
no
not be at
thrist
at any time

(no not shall hunger +++ Ne legyen éhes)
```

10. John 6:41 I AM the Bread that came down out of heaven.

```
were grumbling
therefore the Jews
about Him
because
He said
I AM
the Bread 
(the)
having come down
out of heaven
```

11. John 6:48 I AM the Bread of Life.

```
I AM
the
Bread of Life
```

12. John 6:51 I AM the living Bread that came down out of heaven.

```
I AM
the Bread
the
One
out of
heaven
having come down
if anyone shall have eaten
out of this
the Bread
he will live
into the age
and the Bread
that
I will give
is the flesh of Me
for the Life of the world
```

13. John 8:12 I AM the Light of the world.

```
then again
to them
spoke
the
Jesus
saying
I AM
the Light
of the world
the one following 
Me
no not be
walk in the dark
but will have
the Light of Life
```

14. John 8:18 I AM HE WHO testifies about Myself (and the Father testifies als).

```
I AM
the One
bearing witness
concerning
Myself
and bears witness
concerning
Me
the having sent
Me
Father

(I AM has sent you)
(I AM WHO I AM)
(Holy One)
(the spirit of prophecy
is the Testimony of Jesus
Rvelation 19:10)
```

15. John 8:24 Unless you believe that I AM, you will die in your sins.

```
I said therefore to you that
you will die
in the sins of you
because if
not
you believe that
I AM
you will die
in the sins of you
```

16. John 8:28 When you lift up the Son of Man, the you will know that I AM.

```
said
then
to them
the
Jesus
when you shall have lifted up
the Son of Man
then you will know
that
I AM
and
from
Myself
I do
nothing
but as taught
Me
the Father
these things
I speak
```

17. John 8:58 Before Abraham was born, I AM.

```
said
to them
Jesus
truly
truly
I say
to you
before
Abraham
existed
I AM
```

18. John 10:7 I AM the Door of the sheep.

```
said
then
to them
again
the
Jesus
truly
truly
I say
to you
that
I AM
the
Door
of the
sheep
```

19. John 10:9 I AM the Door. If anyone enters through Me, he will be saved.

```
I AM
the
Door
through
Me
if
anyone
enters in
he will be saved
and
he will go in
and
will go out
and
pasture
will find
```

20. John 10:11 I AM the Good Shepherd.

```
I AM
the Shepherd
the Good
the Shepherd
the Good
the soul of Him
lays down
for the sheep

(ÉN VAGYOK a Pásztor a Jó.
A Pásztor a Jó
a lelkét adja
oda
a juhokért)
(No one is Good, but God Mark 10:18)
```

21. John 10:14 I AM the Good Shepherd.

```
I AM
the Shepherd
the Good
and
I know the ones
of Me
and
I am known
of Me
by the ones

(ÉN VAGYOK a Pásztor a Jó 
Én ismerem a belölem valókat
és
Engem ismernek a belölem valókól)
```

22. John 11:25 I AM the Resurrection and the Life.

```
said to her
the Jesus
I AM
the Resurrection
and
the Life
the one
believing
in
Me
even if
should die
will live
```

23. John 13:19 I AM HE (the Expected One, the Saviour).

```
from now
I say
to you
before
it comes to pass
so that
you will believe
when
it comes to pass
that
I AM
```

24. John 14:6 I AM the Way, the Truth, and the Life.

```
says to him
the Jesus
I AM
the Way
and
the Truth
and
the Life
no one comes
to the Father
except
through
Me
```

25. John 15:1 I AM the True Vine.

```
I AM
the Vine
the True
and
the Father
of Me
is the
Vinedresser
```

26. John 15:5 I AM the Vine

```
I AM
the Vine
you are the branches
the one abiding
in Me
and I
in the one
therefore bears fruit
becuase
apart from
Me
not
you are able
to do
nothing
```

27. John 18:5 I AM Jesus the Nazarene

```
they answered
Him
Jesus
of
Nazareth
He says
to them
I AM
had been standing
however also
Judas who
is delivering up
Him
with
them
```

28. John 18:6 I AM Jesus the Nazarene

```
when
then
He said
to them
I AM
they drew
toward
the back
and fell
to the ground
```

29. John 18:8 I AM Jesus the Nazarene

```
answered
Jesus
I have told you
that
I AM
if therefore
Me
you seek
allow these
to go away
```

30. Acts 9:5 Jesus appears to Paul: "I AM Jesus whom you are persecuting.".

```
he said
and
who are You
Lord
the
and
I AM
Jesus
whom you
are persecuting
```

31. Acts 18:10 Jesus to Paul at Corinth: "I AM with you. Do not be silent.".

```
because
I AM
with you
and
no one
will lay a hand on
you
of
to harm
you
because
people
there are
to Me
many
in the city this
```

32. Acts 22:8 Paul testifies to Jews in Jerusalem: "He said: I AM Jesus.".

```
I
and
answered
who are You
Lord
He said
to me
I AM
Jesus of Nazareth
whom you
are persecuting
```

33. Acts 26:15 Paul testifies before King Agrippa: "He said: I AM Jesus."

```
I
and
said
who are You
Lord
the
and
Lord said
I AM
Jesus
whom you are persecuting
```

34. Revelation 1:8 I AM the Alpha and Omega, who is and was and is to come, the Almighty

```
I AM
the Alpha
and
the
Omega
says
the Lord
the God
the One
Who is
and
the was
and
the is to come
the Almighty
```

35. Revelation 1:17 I AM the First and the Last, the Livining One.

```
and when
I saw
Him
I fell
to the feet
of Him
as if
dead
and
He placed
the right hand
of Him
upon me
saying
do not fear
I AM
the First
and
the Last
```

36. Revelation 2:23 I AM HE WHO searches minds and hearts.

```
and the children of her
I will destroy in death
and will know
all the churches that
I AM
the
searching
affections and hearts
and
I will give
to you
each according to
the works of you
```

37. Revelation 22:16 I AM the Root and Seed of David, the bright Morning Star.

```
I
Jesus
have sent
the angel
of Me
to testify
to you
these
in the churches
I AM
the root and the
offspring of David
the Star
the Bright
the Morning
```
