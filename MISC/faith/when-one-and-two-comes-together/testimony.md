## The Purpose of the Testimony

## 1 Corinthians 14:1-4

```
pursue
the
love
be zealous for
[and]
the
spiritual (ΠΝΕΥΜΑΤΙΚΑ) gifts
especially
[and]
that
you may
prophesy (ΠΡΟΦΗΤΕΥΗΤΕ)

2
the one
for
speaking
[in] a tongue (ΓΛΩΣΣΗΙ)
not to mean speaks
[to] God (ΘΕΩΙ)
no one
for
hears [him]
[in the] Spirit (ΠΝΕΥΜΑΤΙ)
however
he speaks
mysteries (ΜΥΣΤΗΡΙΑ)

3
the one
and
prophesying (ΠΡΟΦΗΤΕΥΩΝ)
[to] men
speaks
edification
and
exhortation ΠΑΡΑΚΛΗΣΙΝ {encouragement}
and
consolation

4
the one
speaking [in] a tongue (ΓΛΩΣΣΗΙ)
himself edifies
the one
and
prophesying (ΠΡΟΦΗΤΕΥΩΝ)
[the] church edifies
```

## Proverbes 19:21

```
many
[are the] plans
in [the] heart
[of] man
and [the] counsel
[of] the LORD (יהוה)
it
will arise
```

```
let us
```

## Hebrews 10:23-24

```
we should hold fast
[to] the
confession (ΟΜΟΛΟΓΙΑΝ)
[of our]
Hope (ΕΛΠΙΔΟΣ)
firmly faithful
for
the One
having promised (ΕΠΑΓΓΕΙΛΑΜΕΝΟΣ) {evangeliumhoz}
```

## Hebrews 12:28

```
therefore
[receiving] a Kingdom
[which] cannot be shaken
receive 
we
grace
through which
we serve
acceptably
the God (O ΘΕΟΣ)
with reverence
and
awe
```

## Hebrews 13:15-16

```
through Him
we should offer
[the] sacrifice
[of] praise
through ecerything
That is [to] God (ΤΩΙ ΘΕΩΙ)
[the] fruit
[of] lips
confessing (ΟΜΟΛΟΓΟΥΝΤΩΝ)
the Name (ΟΝΟΜΑΤΙ)
of Him

{Ö rajta keresztül felajánljuk
az áldozati áldást mindenen keresztül
a gyümölcse az ajkaknak:
A hitvallása az Ö Nevének} CSIA

16
of the
and
good doing
and
sharing
do not be forgetful
with such
for
sacrifices
is well pleased
the God (O ΘΕΟΣ)
```

## Hebrews 13:19

```
abundantly
and
I exhort (ΠΑΡΑΚΑΛΩ)
this
to do
so that
more speedily
I may be restored
to you
```

[Hebrews 4 whole chapter](https://www.livinggreeknt.org/NTbooks/hebrews/hebrews4.php)

## Hebrews 4:12-14

```
living
for
the
Word (ΛΟΓΟΣ) [of] God (ΘΕΟΥ)
and active
and sharper
than any sword
two-edged
and
penetrating
as far as
[the] division
[of] soul (ΨΥΧΗΣ)
and
spirit (ΠΝΕΥΜΑΤΟΣ)
[of] joints
and also marrows
and able to judge (ΚΡΙΤΙΚΟΣ)
the thoughts
and
intensions
[of the] heart
```

## As a crossreference from God given unto iam+from+india while we as two who came togather in Jesus Name are speaking

## Romans 8:26-28 

```
likewise
and
also
the
Spirit (ΠΝΕΥΜΑ)
joins to help
the weakness of us
the
for
[things] which
we should pray for (ΠΡΟΣΕΥΞΩΜΕΘΑ)
s it is proper
not we know
but He the
Spirit (ΠΝΕΥΜΑ)
makes intercession with
groanings unutterable

27
the One
and
seraching
the hearts knows
what [is] the mind (ΦΡΟΝΗΜΑ)
[of] the
Spirit (ΠΝΕΥΜΑΤΟΣ)
because accordin to
God (ΘΕΟΝ)
He intercedes
for
[the] saints

28
we know
and
that
[to] the [ones]
loving (ΑΓΑΠΩΣΙ)
from
God (ΘΕΟΝ)
all [things]
works together
unto good
[to] the [ones]
according to
pupose
called
being
(I AM ΟΥΣΙΝ) 
```

## Romans 10:9

```
that if
you are confessing (ΟΜΟΛΟΓΗΣΗΙΣ)
in the mouth of you
[the] Lord (ΚΥΡΙΟΝ) [is] Jesus
and
are believing
in the heart of you that
the God (Ο ΘΕΟΣ)
Him
raised
out of
[the] dead
you will be saved (ΣΩΘΗΣΗΙ)
```

## 2024.10.04

![ mod prayer](2.png)


## 2024.10.07 14:47

![ request for testimony](4.png)


## 2024.10.07 21:56

![confession and proclaim](5.png)


## Conclusion

- The testimony shows: that the Gift of the Holy Spirit: of speaking the unknown tongue. Speaking the mystery. AND requesting with prayer for understanding and interpretation is indeed benefits whoever has ear for listening
- the testimony stands and requires an `AMEN` to be accepted to be true. 
- If this is from God; Let it be understand. If not this is from God; Let it be discarded
- I have a custom to greet with prayer or with a proclaim. I used to send the greet with prayer to multiple persons and changing the words after the proclaim. To target the person who reads it.

![custom of greeting](5.png)

- The Word of God prayer that cause brother iam-from-india and brother kappa to come together and proclaim to work of the LORD

![Father wills it: so we do this or that](7.png)

## Isaiah 43:11-12

```
"I, [only]
I, am the Lord,
And there is no Savior besides Me.
"I have declared [the future]
and saved [the nation]
and proclaimed [that I am God],
And there was no strange (alien) god among you;
Therefore you are My witnesses [among the pagans],"
declares the Lord, "That I am God.
```


## Isaiah 63:8

```
For He said,
"Be assured,
they are My people,
Sons who will not be faithless."
So He became their
Savior [in all their distresses].
```

## 2 Timothy 3:16-17

```
All Scripture is God-breathed [given by divine inspiration]
and is profitable for instruction,
for conviction [of sin],
for correction [of error and restoration to obedience],
for training in righteousness
[learning to live in conformity to God's will,
both publicly and privately—behaving
honorably with personal integrity
and moral courage];
so that the man of God may be complete
and proficient, outfitted
and thoroughly equipped for every good work.
```

## 1 Corinthians 14:13

```
Therefore let one who speaks in a tongue
pray that he may [be gifted to]
translate or explain [what he says].
```

## 1 Corinthians 14:38-40

```
If anyone does not recognize this
[that it is a command of the Lord],
he is not recognized [by God].
Therefore, believers,
desire earnestly to prophesy
[to foretell the future,
to speak a new message from God to the people],
and do not forbid speaking in unknown tongues.
But all things must be done appropriately
and in an orderly manner.
```

## Galatians 5:22-24

```
But the fruit of the Spirit
[the result of His presence within us]
is love [unselfish concern for others],
joy,
[inner] peace,
patience [not the ability to wait, but how we act while waiting],
kindness,
goodness,
faithfulness,
gentleness,
self-control.
Against such things there is no law.
And those who belong to Christ Jesus
have crucified the sinful nature together
with its passions and appetites.
```

## Romans 8:14-15

```
For all who are allowing themselves
to be led by the Spirit of God
are sons of God. For you have not received
a spirit of slavery leading
again to fear [of God's judgment],
but you have received the Spirit of adoption
as sons [the Spirit producing sonship]
by which we [joyfully] cry, "Abba! Father!"
```

## Jude 1:20-25

```
But you, beloved, build yourselves up on [the foundation of]
your most holy faith
[continually progress, rise like an edifice higher and higher],
pray in the Holy Spirit,
and keep yourselves in the love of God,
waiting anxiously and looking forward to
the mercy of our Lord Jesus Christ
[which will bring you] to eternal life.
And have mercy on some, who are doubting;
save others, snatching them out of the fire;
and on some have mercy but with fear,
loathing even the clothing spotted
and polluted by their shameless immoral freedom.
Now to Him who is able to keep you from stumbling
or falling into sin, and to present you unblemished
[blameless and faultless]
in the presence of His glory
with triumphant joy and unspeakable delight,
to the only God our Savior,
through Jesus Christ our Lord,
be glory, majesty, dominion,
and power, before all time
and now and forever. Amen.
```



## HELPER section
tongue
ΓΛΩΣΣΗΙ 'glosei'

breath
ΠΝΕΥΜΑΤΙΚΑ 'pnevmatika'
ΠΝΕΥΜΑΤΙ 'pnevmati'
ΠΝΕΥΜΑΤΟΣ 'pnevmatosz'
ΠΝΕΥΜΑ 'pnuma' {lélek}

prophetic
ΠΡΟΦΗΤΕΥΗΤΕ 'profocié'
ΠΡΟΦΗΤΕΥΩΝ 'profétion'
ΠΡΟΣΕΥΞΩΜΕΘΑ 'proszexofena' {ima téma}

pray
ΠΑΡΑΚΛΗΣΙΝ 'paraklészin' {imádkozni}
ΠΑΡΑΚΑΛΩ 'párákállo' {kérem}

God
ΘΕΩΙ 'feoi'
ΤΩΙ ΘΕΩΙ 'toi feoi'
Ο ΘΕΟΣ 'oh theos'
ΟΥΣΙΝ {I AM}

promise
ΕΠΑΓΓΕΙΛΑΜΕΝΟΣ 'epangeliamenos'

Mysteri
ΜΥΣΤΗΡΙΑ 'misztéria'
ΕΛΠΙΔΟΣ 'elpidosz'
ΛΟΓΟΣ 'logos'

proclaim 
ΟΜΟΛΟΓΙΑΝ 'omoloján' {confession}
ΟΜΟΛΟΓΟΥΝΤΩΝ 'omolohudon' {hitvallok}
ΟΜΟΛΟΓΗΣΗΙΣ 'omologisis' {vallomás, testimony}
