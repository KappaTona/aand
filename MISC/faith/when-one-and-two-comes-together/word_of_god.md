The Word of God, is a very great sum up of the Story of the Bible. Literally a Word, Just One Holy Word. And the Word become Flesh and The Word of God will not pass ever and Lives! In My Lord and God Who Save Name; Amen.

The Word Says: I AM is The Living God and Everloving Heavenly Father! WHO is sent to us, His only Son! So that whoever calls upon His Holy Name; The Name above ALL names! Is Saved. Amen Again.

The Holy Promise proclaims: To have life that is eternal is to Know The Father.
The Word Says: I AM is rising My Horn, in My Nation! And I gather My sons and daughters from the four sides of the Earth and Love them! Free.


The Word Says: He WHO I AM sent is born! As the Prophets, proclaimed. He is Called according to His Holy Spirit and received and begotten and Loved. The Son of God is The Heavenly Father pleasure. And we human beings should listen with hearing ears and do with Living Spirit; what The Word of God says.


The Word of God Says: God WHO Save, Live(s)! After He received The Promise of the Living God from His Father. He is tempted in the Desert for forty days. He is WHO full+God and full+Man was tempted, and resisted. No shortcut gained. Praise the LORD!


The Word of God Says: He WHO is Proclaimed as Son! Lives and is hated by those who Dead in the Spirit and does not care. Therefore it was allowed: So that the Prophets proven to speak The Word of God, flawlessly. He suffered. So that when we humans say: Lord, you do indeed understand. Stands True in Your Holy Rock. Amen


The Word of God Says: He WHO died Lives and Lives forever more! As the Prophets of the Living Word Victoriously proclaimed.
Therefore

Have Faith! Abundantly! And Pray: So that the Father and His Son come and personally dwells with each of us.

Pray: For guidance.

Surrender: Your emotions, feelings, sufferings, dizziness.

Proclaim and Prolong your relationship with The Father Through His Son with The Holy Spirit in Jesus Name! Amen.


+++ refernces +++

The Word Says: He WHO I AM sent is born! As the Propeths, proclaimed. He is Called according to His Holy Spirit and received and begotten and Loved. The Son of God is The Heavenly Father pleasure. And we human beings should listen with hearing ears and do with Living Spirit; what The Word of God says.

## The Word Says

Word = John 1:1 AMP

```
In the beginning [before all time] was the Word (Revelation 19; Christ),
and the Word was with God, and [b]the Word was God Himself.

b: In this phrase, “God” appears first in the Greek word order,
emphasizing the fact that “the Word” (Christ) was God—so “God Himself.”
```

## Say: example:  1 Kings 12:22 AMP

```
But the word of God came to Shemaiah the man of God, saying, 
```

## Gift of the Word: example: 1 Timothy 4:4-5 AMP

```
 4 For everything God has created is good,
 and nothing is to be rejected if it is received with gratitude;
 5 for it is sanctified [set apart, dedicated to God] by means of the word of God and prayer.
```

## He WHO I AM sent is born

He = Jesus

WHO I AM = God Almighty Exodus 3:14 AMP

```
God said to Moses, “[c]I Am Who I Am”; and He said,
“You shall say this to the Israelites, ‘I Am has sent me to you.’” 

c: Related to the name of God, YHWH, rendered Lord,
which is derived from the verb HAYAH, to be.
```

He WHO I AM = Jesus, God Almighty in the Father and the Father in the Son

He WHO I AM sent: Jesus, God Almighty in the Father and the Father in the Son by the Holy Spirit.

by the Holy Spirit: Matthew 1:19-25 AMP

```
And Joseph her [promised] husband, being a just and righteous man
and not wanting to expose her publicly to shame, planned to send her away
and divorce her quietly. 20 But after he had considered this,
an angel of the Lord appeared to him in a dream, saying,
“Joseph, descendant of David, do not be afraid to take Mary as your wife,
for the Child who has been Begotten in her is of the Holy Spirit.
21 She will give birth to a Son, and you shall name Him Jesus (The Lord is salvation),
for He will [l]save His people from their sins.” 22 All this happened in order to fulfill
what the Lord had spoken through the [m]prophet [Isaiah]:
23 “Behold, the [n]virgin shall be with child and give birth to a Son,
and they shall call His name Immanuel”—which, when translated, means,
“God with us.” 24 Then Joseph awoke from his sleep and did as the angel of the Lord had commanded him,
and he took Mary [to his home] as his wife,
25 but he kept her a virgin until she had given birth to a Son [her firstborn child];
and he named Him Jesus (The Lord is salvation).

l: Those who, by personal faith, accept Him as Savior are saved from the penalty of sin and reconciled with the Father.
m: The prophets in the Bible always have the ability to foretell the future as revealed to them by God.
Scripture provides stringent criteria for testing a prophet’s ability to foretell future events (Deut 18:22).
n: “Virgin” (Gr parthenos) clearly confirms that Mary was a virgin when she gave birth to Jesus.

Deuteronomy 18:22 AMP
When a prophet speaks in the name of the Lord and the thing does not happen or come true,
that is the thing which the Lord has not spoken. The prophet has spoken it presumptuously;
you shall not be afraid of him.
```

## ArchAngel greeting: Luke 2:13-14 AMP

```
Then suddenly there appeared with the angel a multitude of the heavenly host (angelic army) praising God and saying,
14 
“Glory to God in the highest [heaven],
And on earth peace among men with whom He is well-pleased.”
```

## As the Propeths, proclaimed

Proclaim: Psalms 118:15-17 AMP

```
15
The sound of joyful shouting and salvation is in the tents of the righteous:
The right hand of the Lord does valiantly.
16 
The right hand of the Lord is exalted;
The right hand of the Lord does valiantly.
17 
I will not die, but live,
And declare the works and recount the illustrious acts of the Lord.
```

## The Word of God came to Propeth Jeremiah and said: Jeremiah 28:12-14 AMP

```
The word of the Lord came to Jeremiah [some time] after
Hananiah the prophet had broken the yoke off the neck of the prophet Jeremiah,
saying, 13 “Go and tell Hananiah,
‘The Lord says this, “You have broken yokes of wood, but you have made in their place bars of iron.”
14 For thus says the Lord of hosts, the God of Israel,
“I have put the iron yoke [of servitude] on the neck of all these nations,
that they may serve Nebuchadnezzar king of Babylon; and they will serve him.
And I have even given him the beasts of the field.”’” 
```

## Propeths Calling: Amos 7:14-16 AMP

```
Then Amos replied to Amaziah, “I am not a prophet [by profession], nor am I a prophet’s son;
I am a herdsman and a grower of sycamore figs.
15 But the Lord took me as I followed the flock and the Lord said to me,
‘Go, prophesy to My people Israel.’ 16 Now therefore, listen to the word of the Lord:
You say, ‘You shall not prophesy against Israel nor shall you speak against the house of Isaac.’ 
```

## Propeth proclaimed: He WHO I AM sent is born Isaiah 7:13-14 AMP

```
Then Isaiah said, “Hear then, O house of David! Is it too small a thing for you to try the patience of men,
but will you try the patience of my God as well?
14 Therefore the Lord Himself will give you a sign:
Listen carefully, the [f]virgin will conceive and give birth to a son,
and she will call his name Immanuel (God with us).

f: This prophecy of the virgin is declared in Matt 1:22, 23 to be fulfilled in the birth of Jesus.
There has been a great deal of discussion over the Hebrew word found here for virgin (almah)
and the word that Matthew uses (parthenos). The latter refers unambiguously to a virgin,
while the former (almah) has been said to refer to a young woman,
in contrast to the Hebrew word bethulah, which is the equivalent of the Gr parthenos.
It has also been noted that the Septuagint, the Greek translation of the Hebrew OT,
has parthenos here for almah, and that Matt 1:23 is taken from the Septuagint.
Some have wondered why the Septuagint translators used the more specific word parthenos.
It is fair to say that this question is the result of oversimplifying the vocabulary
and misinterpreting the distinctions. The Hebrew words almah and bethulah can actually
refer to the same kind of woman; almah is a youthful woman of marriageable age,
one who has not yet had her first child, while bethulah is one who has not been touched
in an intimate way. Furthermore, in the present context it would be unthinkable to infer
that the woman might have had sexual relations outside of marriage. So the well-known
translation of “young woman” for almah, while technically not incorrect, can be viewed as
too ambiguous for the Hebrew word and the context. Parthenos was an appropriate choice in the Greek.
Another word, kore (for “girl”) could have been used, but it has a wider range of meaning
than the Heb almah (Mark uses a related word, korasion, to translate Jesus’ Aramaic word talitha).
It should also be acknowledged from a theological perspective that
when Matthew cites the verse with parthenos, he thereby authenticates it as inspired.
```

## He is Called according to His Holy Spirit

He = Jesus

Called: example how Samuel was Called according to the Holy Spirit: 1 Samuel 3:1-11 AMP

```
1
Now the boy Samuel was attending to the service of the Lord [a]under the supervision of Eli. The word of the Lord was rare and precious in those days; visions [that is, new revelations of divine truth] were not widespread.

2 Yet it happened at that time, as Eli was lying down in his own place
(now his eyesight had begun to grow dim and he could not see well).
3 and the [oil] lamp of God had not yet gone out,
and Samuel was lying down in the temple of the Lord,
where the ark of God was, 4 that the Lord called Samuel,
and he answered, “Here I am.” 5 He ran to Eli and said,
“Here I am, for you called me.” But Eli said,
“I did not call you; lie down again.” So he went and lay down.
6 Then the Lord called yet again, “Samuel!”
So Samuel got up and went to Eli and said,
“Here I am, for you called me.” But Eli answered,
“I did not call, my son; lie down again.”
7 Now Samuel did not yet know [or personally experience] the Lord,
and the word of the Lord was not yet revealed [directly] to him.
8 So the Lord called Samuel a third time. And he stood and went to Eli and said,
“Here I am, for you did call me.”
Then Eli understood that it was the Lord [who was] calling the boy.
9 So Eli said to Samuel, “Go, lie down, and it shall be that if He calls you,
you shall say, ‘Speak, Lord, for Your servant is listening.’”
So Samuel went and lay down in his place.

10 Then the Lord came and stood and called as at the previous times,
“Samuel! Samuel!” Then Samuel answered,
“Speak, for Your servant is listening.”
11 The Lord said to Samuel,
“Behold, I am about to do a thing in Israel at which both ears of everyone who hears it will ring.
```

## How Jesus was receiving His Holy Spirit Calling: Mark 1:1-2 AMP Mark 1:7-8 AMP Mark 1:9-11 AMP

```
The beginning of the [facts regarding the] good news of [a]Jesus Christ, the Son of God.

2 As it is written and forever remains in the [writings of the] prophet Isaiah:

“Behold, I send My messenger ahead of You,
Who will prepare Your way—

And he was preaching, saying,
“After me comes He who is mightier [more powerful, more noble] than I,
and I am not worthy to stoop down and untie the straps of His sandals
[even as His slave]. 8 As for me, I baptized you [who came to me] with water [only];
but He will baptize you [who truly repent] [e]with the Holy Spirit.”

In those days Jesus came from Nazareth of Galilee and was baptized by John in the Jordan.
10 [f]Immediately coming up out of the water, he (John) saw the heavens torn open,
and the [g]Spirit like a dove descending on Him (Jesus);
11 and a [h]voice came out of heaven saying:
“You are My beloved Son, in You I am well-pleased and delighted!”

a: Mark’s gospel is believed to be directed primarily to Gentile believers in Rome.
Since Gentiles would not necessarily be familiar with the Jewish title Messiah,
he emphasizes instead the fact that Jesus is the Son of God.
e: The Greek here can be translated: [with, in, or by.]
g: The three persons of the Godhead were present: the Father, the Son, and the Holy Spirit.
h: See note Matt 3:17.:
[Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
Also see Matt 17:5; Mark 1:11; 9:7; Luke 3:22; 9:35; John 12:28.
```

## The notes from the above [h] quote

Matthew 17:5 AMP

```
While he was still speaking, behold, a bright cloud overshadowed them,
and [a]a voice from the cloud said,
“[b]This is My beloved Son, with whom I am well-pleased and delighted! Listen to Him!

a: [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
b: The Father’s words were a reminder that Jesus is unique,
not to be equated with Moses and Elijah. He alone is the Son.
The focus is on Him.
```

Mark 9:7 AMP

```
Then a cloud formed, overshadowing them, and [b]a voice came out of the cloud, “This is My beloved Son. Listen to Him and obey Him!”

b: [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
```

Luke 3:22 AMP

```
and the [l]Holy Spirit descended on Him in bodily form like a dove, and a voice came from heaven, “You are My Son, My Beloved, in You I am well-pleased and delighted!”

l: [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
```

Luke 9:35 AMP

```
Then [e]a voice came out of the cloud, saying, “This is My beloved Son, My Chosen One; listen and obey and yield to Him!”

e: [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
```

John 12:28 AMP

```
 [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a voice came from heaven saying, “I have both glorified it,
and will glorify it again.”

```


## How Jesus was begotten: Hebrews 1:3-5 AMP

```
The Son is the radiance and only expression of the glory of [our awesome] God
[reflecting God’s [a]Shekinah glory, the Light-being,
the brilliant light of the divine],
and the exact representation and perfect imprint of His [Father’s]
essence, and upholding and maintaining
and propelling all things [the entire physical and spiritual universe]
by His powerful word [carrying the universe along to its predetermined goal].
When He [Himself and no other] had [by offering Himself on the cross as a sacrifice for sin]
accomplished purification from sins and established our freedom from guilt,
He sat down [revealing His completed work] at the right hand of the Majesty on high
[revealing His Divine authority],
4 having become as much superior to angels,
since He has inherited a more excellent and glorious
[b]name than they [that is, Son—the name above all names].

5 For to which of the angels did the Father ever say,

“You are My Son,
Today I have begotten (fathered)
You [established You as a Son, with kingly dignity]”?

And again [did He ever say to the angels],

“I shall be a Father to Him
And He shall be a Son to Me”?


a: The word “Shekinah” does not appear in Scripture,
but has been used by both Christians and Jews to describe the visible divine Presence of God,
in such things as the burning bush,
the cloud and the pillar of fire that led the Hebrews
in the wilderness, and the Presence of God that
rested between the cherubim over the mercy seat of the ark.
b: In Greek “name” occurs last in this verse to emphasize
that Jesus alone bears the name Son. No angel is superior to the Son.
```

## How Jesus was loved: Matthew 3:17 AMP

```
See note Matt 3:17.:
[Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’”
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
Also see Matt 17:5; Mark 1:11; 9:7; Luke 3:22; 9:35; John 12:28.
```


## The Son of God is The Heavenly Father pleasure: Matthew 3:17 AMP , Mark 1:11 AMP

Matthew 3:17 AMP
Mark 1:11 AMP
```
and behold, a [m]voice from heaven said,
“This is My beloved Son, in whom I am well-pleased and delighted!”

m: [Rather, I will say,] ‘Father, glorify (honor, extol) Your name!’” 
Then a [d]voice came from heaven saying, “I have both glorified it,
and will glorify it again.”
```


## The very first thing the Holy Spirit told Jesus to do, and He did: Mark 1:12 AMP

```
Immediately the [Holy] Spirit forced Him out into the wilderness.
13 He was in the wilderness forty days being tempted [to do evil] by Satan;
and He was with the wild animals, and the angels ministered continually to Him.
```

Listen to the Voice: Hebrews 12:25-26 AMP , Luke 21:38 AMP , Acts 7:39-40 AMP , Mark 9:5-8 AMP , James 1:19 AMP

```
See to it that you do not refuse [to listen to] Him who is speaking [to you now].
For if those [sons of Israel] did not escape when they refused [to listen to]
him who warned them on earth [revealing God’s will], how much less will we escape
if we turn our backs on Him who warns from heaven? 26 His voice shook the earth
[at Mount Sinai] then, but now He has given a promise, saying,
“Yet once more I will shake not only the earth, but also the [starry] heaven.”


And early in the morning all the people would come to Him in the temple to listen to Him.

Our fathers were unwilling to be subject to him [and refused to listen to him].
They rejected him, and in their hearts turned back to Egypt.
40 They said to Aaron, ‘Make for us gods who will go before us;
for this Moses who led us out of the land of Egypt,
we do not know what has happened to him.’ 


Peter responded and said to Jesus,
“Rabbi (Master), it is good for us to be here;
let us make three [sacred] tents—one for You,
and one for Moses, and one for Elijah.”
6 For he did not [really] know what to say because they were terrified
[and stunned by the miraculous sight].
7 Then a cloud formed, overshadowing them,
and [b]a voice came out of the cloud,
“This is My beloved Son.
Listen to Him and obey Him!”
8 Suddenly they looked around and no longer saw anyone with them, except Jesus alone.

Understand this, my beloved brothers and sisters.
Let everyone be quick to hear [be a careful, thoughtful listener],
slow to speak [a speaker of carefully chosen words and],
slow to anger [patient, reflective, forgiving]; 

```

## Doers of the Word of God: James 1:22-25 AMP

```
But prove yourselves doers of the word [actively and continually obeying God’s precepts],
and not merely listeners [who hear the word but fail to internalize its meaning],
deluding yourselves [by unsound reasoning contrary to the truth].
23 For if anyone only listens to the word [f]without obeying it,
he is like a man who looks very carefully at his natural face in a mirror;
24 for once he has looked at himself and gone away,
he immediately forgets [g]what he looked like.
25 But he who looks carefully into the perfect law,
the law of liberty, and faithfully abides by it,
not having become a [careless] listener who forgets but
[h]an active doer [who obeys], he will be blessed
and favored by God in what he does [in his life of obedience].

f: Literally: and is not a doer.
g: Literally: what sort he was.
h: Literally: a doer of work.

```

+++ references +++

The Word of God Says: He WHO is Proclaimed as Son! Lives and is hated by those who Dead in the Spirit and does not care. Therefore it was allowed: So that the Prophets proven to speak The Word of God, flawlessly. He suffered. So that when we humans say: Lord, you do indeed understand. Stands True in Your Holy Rock. Amen

## The Word of God Says: He WHO is Proclaimed as Son!

- The Word of God Says John 1
- He WHO is Proclaimed as Son! Hebrews 1


## He WHO is Proclaimed as Son! Lives:

John 6:35

```
35 Jesus replied to them,
“[a]I am the Bread of Life.
The one who comes to Me will never be hungry,
and the one who believes
in Me [as Savior] will never be thirsty
[for that one will be sustained spiritually].

[a] Jesus uses the words “I am” over twenty times in this Gospel.
Especially memorable are those places where “I am”
is followed by a metaphor that declares
His deity and His relationship to mankind
as Savior (this is the first of seven such references);
ree 8:12 (Light); 10:9 (Door); 10:11 (Good Shepherd); 11:25 (Resurrection); 14:6 (Way, Truth, Life); 15:5 (Vine).

```

John 11:25-26 AMP

```
25 Jesus said to her,
“[a]I am the Resurrection and the Life.
Whoever believes in (adheres to, trusts in, relies on)
Me [as Savior] will live even if he dies;
26 and everyone who lives and believes
in Me [as Savior] will never die. Do you believe this?” 

The fifth of the memorable “I am” statements. See note 6:35.

```

## Jesus is hated:

John 15:25 AMP

```
But [this is so] that the word which
has been written in their Law would be fulfilled,
‘They hated Me without a cause.’
```

Matthew 10:22 AMP 

```
22 And you will be hated by everyone because of
[your association with]
My name, but it is the one who has patiently
persevered and endured to the end who will be saved.
```

## by those who does not care:

Galatians 4:16-17 AMP

```
So have I become your enemy by telling you the truth?
17 These men [the Judaizers]
eagerly seek you [to entrap you with honeyed words and attention,
to win you over to their philosophy],
not honorably [for their purpose
is not honorable or worthy of consideration].
They want to isolate you [from us who oppose them] so that you will seek them. 
```

## Jesus is hated so that the Prophets proven to speak The Word of God

John 15:26 AMP

```
“But when the [a]Helper
(Comforter, Advocate, Intercessor—Counselor, Strengthener, Standby)
comes, whom I will send to you from the Father,
that is the Spirit of Truth who comes from the Father,
He will testify and bear witness about Me.


[a]
Greek Paracletos, one called alongside to help.
```

## Jesus is the Holy Rock

Isaiah 26:4 AMP

```
“Trust [confidently] in the Lord forever
[He is your fortress, your shield, your banner],
For the Lord God is an everlasting Rock [the Rock of Ages].
```

Psalms 73:26 AMP

```
My flesh and my heart may fail,
But God is the rock
and strength of my heart and my portion forever.
```

Luke 6:48 AMP

```
he is like a [far-sighted, practical, and sensible]
man building a house,
who dug deep and laid a foundation on the rock;
and when a flood occurred,
the torrent burst against that house and yet could not shake it,
because it had been securely built and founded on the rock.
```

## Speaking Word of God flawlessly

Deuteronomy 18:22 AMP

```
When a prophet speaks in the name of the Lord
and the thing does not happen or come true,
that is the thing which the Lord has not spoken.
The prophet has spoken it presumptuously; you shall not be afraid of him.
```

## Spiritually not Dead:

1 John 4:15-17 AMP

```
15 Whoever confesses and acknowledges that Jesus is the Son of God,
God abides in him, and he in God.
16 We have come to know [by personal observation and experience],
and have believed [with deep, consistent faith]
the love which God has for us.
God is love, and the one who abides in love abides in God,
and God abides continually in him.
17 In this [union and fellowship with Him],
love is completed and perfected with us,
so that we may have confidence in the day of judgment
[with assurance and boldness to face Him]; because as He is, so are we in this world. 
```


## He suffered as the Prophet Isaiah Spoke the Word of God says so

Propeth Isaiah description by the Word of God: About Jesus the Son of God, The Messiah, The Anointed, The Holy One.
Isaiaha 53 AMP (whole chapter)



+++ references +++

```
Abundantly! And Pray: So that the Father and His Son come and personally dwells with each of us.
Pray: For guidance.
Surrender: Your emotions, feelings, sufferings, dizziness.
Proclaim and Prolong your relationship with The Father Through His Son with The Holy Spirit in Jesus Name! Amen.
```

## Have Faith! And Pray

Mark 11:12-14 AMP Hunger with Faith.

```
On the next day, when they had left Bethany, He was hungry.
13 Seeing at a distance a fig tree in leaf,
He went to see if He would find anything on it.
But He found nothing but leaves,
for it was not the season for figs.
14 He said to it,
“No one will ever eat fruit from you again!”
And His disciples were listening [to what He said].
```

Mark 11:21-26 AMP Result and Pray

```
And remembering,
Peter said to Him,
“Rabbi (Master), look! The fig tree which You cursed has withered!”
22 Jesus replied, “Have faith in God [constantly].
23 I assure you and most solemnly say to you,
whoever says to this mountain,
‘Be lifted up and thrown into the sea!’
and [a]does not doubt in his heart [in God’s unlimited power],
but believes that what he says is going to take place,
it will be done for him [in accordance with God’s will].
24 For this reason I am telling you, whatever
things you ask for in prayer [in accordance with God’s will],
believe [with confident trust] that you have received them,
and they will be given to you. 25 Whenever you
[b]stand praying, if you have anything against anyone,
forgive him [drop the issue, let it go],
so that your Father who is in heaven will also
forgive you your transgressions and wrongdoings
[against Him and others].
26 [c][But if you do not forgive,
neither will your Father in heaven forgive your transgressions.”]



[a] Jesus used this moment to emphasize to the disciples that a person’s confident,
abiding faith combined with God’s power
can produce absolutely amazing results,
if the request is in harmony with God’s will.
God is fully capable of doing
that which man regards as impossible (cf 14:36; James 4:3).

[b] Standing was a common posture for prayer among the Jews (see Matt 6:5).

[c] Early mss do not contain this verse.
```

## So that the Father and His Son come and personally dwells with each of us.

John 14:1-2 AMP The amount of houses the Father has.

```
“Do not let your heart be troubled (afraid, cowardly).
Believe [confidently] in God and trust in Him,
[have faith, hold on to it, rely on it, keep going and]
believe also in Me.
2 In My Father’s house are many dwelling places.
If it were not so, I would have told you,
because I am going there to prepare a place for you. 
```

John 14:23-24 AMP
```
Jesus answered,
“If anyone [really] loves Me,
he will keep My word (teaching);
and My Father will love him,
and We will come to him
and make Our dwelling place with him.

One who does not [really]
love Me does not keep My words.
And the word (teaching)
which you hear is not Mine,
but is the Father’s who sent Me.
```

## Proclaim and Prolong your relationship with The Father Through His Son with The Holy Spirit in Jesus Name! Amen.

John 14:16-21 AMP Role of the Spirit

```
And I will ask the Father,
and He will give you another [a]Helper
(Comforter, Advocate, Intercessor—Counselor, Strengthener, Standby),
to be with you forever—
17 the Spirit of Truth,
whom the world cannot receive [and take to its heart]
because it does not see Him or know Him, but you know
Him because He (the Holy Spirit)
remains with you continually and will be in you.

18 “I will not leave you as orphans
[comfortless, bereaved, and helpless];
I will come [back] to you.
19 After a little while the world will no longer see
Me, but you will see Me;
because I live, you will live also.
20 On that day [when that time comes]
you will know for yourselves that I am in My Father,
and you are in Me, and I am in you.
21 The person who has My commandments and keeps them is
the one who [really] loves Me; and whoever [really]
loves Me will be loved by My Father,
and I will love him and reveal Myself to him
[I will make Myself real to him].” 

[a]
Greek Paracletos, one called alongside to help.
```

John 5:18-24 AMP The relationship between the Father and Son

```
This made the Jews more determined than ever to kill Him,
for not only was He breaking the Sabbath
[from their viewpoint],
but He was also calling God His own Father,
making Himself equal with God.

19 So Jesus answered them by saying,
“I assure you and most solemnly say to you,
the Son [a]can do nothing of Himself [of His own accord],
unless it is something He sees the Father doing;
for whatever things the Father does,
the Son [in His turn] also does in the same way.
20 For the Father dearly loves the Son and shows Him everything that
He Himself is doing; and the Father will show Him greater works than these,
so that you will be filled with wonder.
21 Just as the Father raises the dead
and gives them life [and allows them to live on],
even so the Son also gives life to whom He wishes.
22 For the Father judges no one,
but has given all judgment [that is, the prerogative of judging]
to the Son [placing it entirely into His hands],
23 so that all will give honor (reverence, homage)
to the Son just as they give honor to the Father.
[In fact] the one who does
not honor the Son does not honor the Father who has sent Him.

24 “I assure you and most solemnly say to you,
the person who hears My word [the one who heeds My message],
and believes and trusts in Him who sent Me,
has (possesses now) eternal life
[that is, eternal life actually begins—the believer is transformed],
and does not come into judgment
and condemnation, but has passed [over] from death into life.


[a]
Jesus uses the analogy of a son learning
a trade from his father to show the religious
leaders that when they condemn what He does,
they are condemning the Father as well.
```

John 4:23-24 AMP Worship in Spirit and in Truth: The Father is Spirit.

```
23But a time is coming and is already here
when the true worshipers will worship the Father
in spirit [from the heart, the inner self]
and in truth; for the Father seeks
such people to be His worshipers.
24 God is spirit [the Source of life, yet invisible to mankind],
and those who worship Him must worship in spirit and truth.” 
```


## Prolong your relationship with God

Ephesians 5:6-10 AMP

```
6 Let no one deceive you with empty arguments
[that encourage you to sin],
for because of these things the wrath of God comes upon
the sons of disobedience [those who habitually sin].
7 So do not participate or even associate with them
[in the rebelliousness of sin]. 8 For once you were darkness,
but now you are light in the Lord;
walk as children of Light [live as those who are native-born to the Light]
9 (for the fruit [the effect, the result]
of the Light consists in all goodness and righteousness and truth),
10 trying to learn [by experience]
what is pleasing to the Lord
[and letting your lifestyles be examples of
what is most acceptable to Him—your behavior
expressing gratitude to God for your salvation]. 
```

## Pray: For guidance

Psalms 25 AMP Prayer for Protection Guidance and Pardon

## Surrender control

Proverbs 3:1-7 AMP

```
My son, do not forget my law,
But let your heart keep my commandments;
2 

For length of days and years of life [worth living]
And tranquility and prosperity [the wholeness of life’s blessings] they will add to you.
3 

Do not let mercy and kindness and truth leave you [instead let these qualities define you];
Bind them [securely] around your neck,
Write them on the tablet of your heart.
4 

So find favor and high esteem
In the sight of God and man.
5 

Trust in and rely confidently on the Lord with all your heart
And do not rely on your own insight or understanding.
6 

[b]In all your ways know and acknowledge and recognize Him,
And He will make your paths straight and smooth [removing obstacles that block your way].
7 

Do not be wise in your own eyes;
Fear the Lord [with reverent awe and obedience] and turn [entirely] away from evil.

[b]
One of the ancient rabbis said that all the essence of the Torah (Law) depends on this verse.
```

## Surrender feelings and dizziness; Lead by the Holy Spirit

Romans 6 AMP; Believers are dead to Sin, Alive in God

Ephesians 5:15-18; Be Imitators of God; Be filled with the Holy Spirit the days are full of evil

```
Therefore see that you walk carefully
[living life with honor, purpose, and courage;
shunning those who tolerate and enable evil],
not as the unwise, but as wise
[sensible, intelligent, discerning people],
16 [a]making the very most of your time
[on earth, recognizing and taking advantage of
each opportunity and using it with wisdom and diligence],
because the days are [filled with] evil.
17 Therefore do not be foolish and thoughtless,
but understand and firmly grasp what the will of the Lord is.
18 Do not get drunk with wine, for that is wickedness
(corruption, stupidity), but be filled with the [Holy] Spirit and constantly guided by Him.

[a]
The Greek word in this verse means
“buy up at the market place.”
Opportunity is regarded
as a commodity to be used by believers.
```

1 Peter 5:6-11; Serve God Willingly; Seek God's Best

```
Therefore humble yourselves under the mighty hand of God
so that He may exalt you at the appropriate time,
casting all your cares on Him for He cares about you.
Be sober, be alert and cautious at all times.
The enemy of yours, the devil, prowls around like
a roaring lion, seeking someone to devour.
But resist him, be firm in faith,
Knowing that the same experiences of suffering are being 
experienced by your brothers and sisters
throughout the world. After you have suffered a little while,
the God of all grace, who called you to His eternal glory in
Christ (The Anointed) will Himself complete,
confirm, strengthen, and establish you.
To Him be dominion forever and ever. Amen.
```

## Proclaim the Works of the LORD

Psalm 118:17 I will not die; but live; and proclaim the works of the lord

```
I will not die, but live,
And declare the works and recount
the illustrious acts of the LORD
```


## Amen
Revelation 3:14 AMP

```
"To the andle of the church in Laodicea write:
These are teh words of the Amen, the trusted
and faithful and true
Witness, the Beginning and Origin of God's Creation:
```

+++ references +++

```
The Word of God Says: God WHO Save, Live(s)!
After He received The Promise of the Living God from His Father.
He is tempted in the Desert for forty days.
He is WHO full+God and full+Man was tempted,
and resisted. No shortcut gained. Praise the LORD!
```

## God WHO save, Live(s)

God WHO save: The LORD is salvation

Matthew 1:25 AMP
```
```

## the Living God

Jeremiah 10:10 AMP

```
```

## Son receive from the Father through the Holy Spirit

John 16:14-15 AMP
```
```

## Temptation and Desert

Mark 1:12-13

```
```

Luke 4:1-13

```
```

## Jesus as Son of Man

Matthew 2:1
Matthew 2:23
Matthew 21:18 hunger
John 4:7 thirsty
John 4:6 Fatigue
Mark 14:34 John 11:35 sorrow
Matthew 9:36 showed compassion
Luke 22:1-23 experienced the pain of betrayal
Romans 5:18–19 Jesus is a perfect representation of what it means to be human

## Jesus as Son of God

Matthew 28:18 He claimed divine authority
Mark 2:5–12 forgave sins
John 2:1–11 performed miracles
Matthew 21:9 accepted worship (Angels of God do not accept worship)
John 10:30 I and the Father are one”
Hebrews 1:1–4  omnipotence 
John 2:25 omniscience 
2 Corinthians 5:21 perfection 
Hebrews 4:15 As fully man, He identifies with our struggles and sympathizes with our weaknesses

## Because Jesus has a dual nature, He can serve as the perfect mediator between God and humanity

1 Timothy 2:5
Matthew 20:28 He possesses the divine authority to redeem humanity from sin
John 11:25 conquer death
Colossians 1:19–20 reconcile us with God
Matthew 7:29 His divine nature gave authority to His teachings 
Hebrews 1:3 In the person of Jesus, we witness the fullest expression of God’s character and nature 


Importantly, He is the sacrificial lamb for our sins.
(John 1:29), and through His life, death,
and resurrection, He secured salvation
for all who believe in Him (Hebrews 7:27).

Through His life and teachings,
Jesus revealed the heart of God—a heart
that is “full of grace and truth” (John 1:14).

