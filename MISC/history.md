HOW TO
---


**SYNOPSIS**

To axiomatize a system of knowledge is to show that its claims

can be derived from small, well-understood set of sentences of **Axiom[s]**.

This is written by **Me** and it is not finished.

**I Believe: †(What I write;)** & always open to change view if **Reason**.

**Word[s]** in bold or between `**Word[s]**` has explanation under the DESCRIPTION section.

*Word[s]* in italits or between `*Word[s]*` has external links in DETAILS section.

For example search for: WORK | POC | CODE | MORE | LESS | ἀξίωμα.

You can read **More** or **Less**.


---

**DESCRIPTION**


**AXIOM**

**Axiom** `<=>` **True** & **Statement**.

Alias for: No need to **Overthink**;


**BASE AXIOMS**

*Act* as **Nature** `<=>` **Axiom**

**Axiom** `<=>` *Logical True* & *fact*

Today **I Believe: Good**; `<=>` *Act* as **Nature** demands; `<=>` **Axiom**;

Today **I Believe: Reason**; `<=>` *Act* as **Nature** demands; `<=>` **Axiom**;


**BODY**

Alias for *Fortitudo*.


**CALL**

Alias: **Operator: †** call.
Alias: implementation detail.
"Értelmezés kérdése" := ?†().

Alias for 
†(DAGGER|CROSS; Affirmation operator; it is true; read; observe; **I Execute: †**;).


**CODE**

The ability to create an executable file from simple words and **Complex Word[s]**.

For **More** **Good Code** **Clean Code**


**COMPLEX WORD[s]**

In this context **Complex Word[s]** are

the keywords & tools provided related *Programming Language*.


**CLEAN CODE**  

**I Believe: Clean Code** does not need any comments, yet still needs a **Manual**.

For **More** **Good Code**


**CONVERGS**

**I Believe: [convergence_logic](https://en.wikipedia.org/wiki/Convergence_(logic)).** <=> **True**

**COST**

**Axiom** `<=>` †(∀: ∃(cost | value);). `<=>` †(Minden hogy van cost vagy value).

Alias for **Operator: #**.


**ETC**

Alias for **Operator: ...**.

And so on.


**EOS**

Alias for end of *Statement*.


**EOSS**

Alias: *I Executed*.
Alias: End Of Soft Statement.
**Operator: ∴**.

For **More**: **Examples**.



**ENV**

Alias for **Operator: λ**. Usage: λ**List item[s]**.
**Env** ~ **Természet** ~ Környezetében.

- λ[∀], λ(∀); := †(minden lista elem λkörnyezetébe;).
- λ[∃], λ(∃); := †(van lista elem λkörnyezetébe;).
- λ[] <=> λ![∃]; := †(nincs lista elem λkörnyezetébe;).

**FACT**

Alias for **Operator: <=>**.

†(A *Statement* `<=>` B *Statement*) `<=>` **True**

A & B `<=>` **Fact**: †(A **Implies** B; & B **Implies** A;)

When stating a **Trivial: Fact** the right side **True** statement is optional.

For **More** *fact*


**FALSE**

Alias for NOT **True**.


**GOOD CODE**

**I Believe: This**:
- **Good Code** `=>` Proven **Code**
- **Good Code** `=>` **Honorable Work**
- **Good Code** `=>` **Clean Code**
- Tested **Code**:= Given industrial standard[s]
- Proven **Code**:= *unittested* & has a **Manual** & has *CI/CD*
- Tested **Code** `<=>` Proven **Code** `<=>` Quality **Code**
- **Good Code** `=>` *Good* *Program*
- exit code equals zero

**TODO: Good Code** in this repository.


**GIT GUD**

"git gud" is a saying that **Practice** makes Master.

For [more](https://www.youtube.com/watch?v=7mbEYCNT5Vk)


**HONORABLE WORK**

This:= (When **I Act: Work** & **I Act: responsibility** for **Work**

& complete it *Good* & with self-sufficing.)

When This happens; **I Believe: Honorable Work** `<=>` **True**

**Honorable Work** `=>` **Work**

For **More** *Honorable*


**HOW TO**

- Alias for **Less**.
- Alias for †(why < how).
- ∃**Results**: **More**|**Less**; & **I Believe: How to**;
- ∃!


**I | ME**

Katona. The commiter.


**I ACT: (Axiom | Act)[s]**

**I** *Act* as **Nature** Demands & **I** *Being* *Human* `<=>` **I Execute: (Axiom | Act)[s]**


**I BELIEVE: Axiom[s]**

Alias for **I Execute** an **Axiom[s]**.

Alias for **Operator: †**.


**I EXECUTE: (Act | Axiom)[s]**

This:= †(**Temperantia**; | *Act* & *Human* & mistakes;).

**I Act: This**'.


**A IMPLIES B**

(A *Statement* `=>` B *Statement*) `<=>` **True**

A **Implies** B := if A true B true; & B NOT **Implies** A;

When stating a **Trivial: Implies** the right side **True** statement is optional.


**LESS**

This file.


**LIST**

**Operator: []**; | **Operator: ()**.


**LIST ITEM[S]**

- [∀], (∀); := †(minden lista elem;).
- [∃], (∃); := †(van lista elem;).
- [] <=> ![∃]; := †(nincs lista elem;).
- () := ∃!|[]|>0.


**MANUAL**

**Manual** is a set of knowledge aimed to be understand a *behavior*.

**Manual** `=>` *Documentation*


**MIND**

Alias for *Prudentia*.


**MISC**

- Alias for Minimal instruction set computer, provessor architecture.
- Alias for **Less** parent folder. 


**MORE**

Alias for See more in DESCRIPTION section.

See DETAILS and BOOKS for even more at the end.


**NATURE -- TERMÉSZET**

**Természet** `<=>` *Legfőbb Jó* `<=>` **Reason** & **tökéletes**.
Ellát megszámlálhatatlanul végtelen sok **Axiom** mellyel *Act* **Gyakorlása** *történik*.


**OVERTHINK**

Alias for thinking on **Axiom[s]**.

See **More** **Practice** **Remind yourself**


**Therefores: [i...]**

Alias **Therefore**: Tehát, Aztán, Hence.
∀[∴, ∴, ∴ ...] =: ∃†(**Therefores**;).

**TL;DR**: ∃i: †(**Logical Symbol** | **Operator[s]...).

**Example**: †([∴]); := **List item: ∴**.


**PRACTICE -- GYAKORLÁS**
Definition: ?†(**Thereforces** **Implies** **Practice**;).

**TL;DR**: λ(∀[∴]: ∃∴ & ∃!∴;).

**Therefores**:
- **I Execute: Practice** ∴ *Good*.
- **I Execute: Practice** ∴ **I Believe: Practice**.
- **Prictice** ∴ **Result**
- †(Trial and Error) ∴ **Practice**

- **I Execute: Practice** **Implies** **I Believe: Practice**.
- **Practice** `=>` **Axiom**.
- **Practice** `=>` Trail and Error.
- **Practice** `=>` Results.
- **Practice** `=>` **git gud**.
- **Practice** `<=>` *Good*


**PRESENT**

- **I Believe: Present**. 
- Let **Present**:= †(constant but not depend on date).


**POC**

**Poc** is this repository. The **Reason** behind its *Being* is to
see if **I** can **Code**.


**QMARK**

†(Let **Operator: ?**; Alias for question; question can be statement;) `=>` ∃$?(question).

α:= ∃$?question.

∃α: **True|False**; => †(α ∴ *Statement*;). question => **Result**;



**A PRODUCES B**

Alias for; A **Implies** B.



**ART OF PROGRAMMING**


Ζ:= **Art of Programming** **Implies** **Programming** **Fact**;
ζ:= $†(Z).
ε:= $†(<< **True**;).
†(Z & ζ;) `<=>` †(**Art of Programming** => **Programming**;).

∀ **More** **Implies** **Programming** **Fact**

**PROGRAMMING**

**I Act: Programming** `=>` Undefined symbol or reference. 

For **More** **Code** **Art of Programming**.


**REASON**

Alias for **I Believe: This**;  This:= *Act* as **Nature** demands.

**Természet**esen *viselkedni*.

†(God ∴ **Reason**):= **Axiom**.


**RESULT: $(True | False)**

**Statement** has a *Logical Consequence* in **True** or **False**.


**RV**

Alias for **Result**. †(Operator:= $) `<=>` **True**.

$†(∃ί: $ί=**Result**;). Usage: $?().


**SPIRIT**

Alias for *Iustitia*.


**STATEMENT**

Alias for *Logical Statement*.

In this section used for assertion that **Result**s.


**TEMPERANTIA**

Alias for (**Body** & **Spirit** & **Mind**).

Sound-minded *act*.


**TMP**

Alias for Temporary.


**TODO**

Alias for **I Act: TODO** soon.


**TRIVIAL: Statement[s]**

Alias for **I Believe: Statement[s]**


**TRUE**

Alias for *Logical Truth*.


**WORK**

**Work** is a task given to **Me**. **Work** is a tool for survival.

**Work** can be *Good* without *Being* **Honorable Work**.

While **Work**ing: **I Believe: This**: 
- Help from collagues are welcomed
- Learning new[s] `=>` *Good*
- Understanding task with cooperation `=>` *Good*.
- Bearing any obstacle `<=>` **I Act: Work**.

For **More** **Honorable Work**.


**WHY**

**Less** *exists*? `<=>` ∃**Less**?
Let α:= †([IdeaSource](https://www.youtube.com/watch?v=UWtbZ9oiZp4)];).
Let ω:= †(**Természet**; `<=>` *Legfőbb Jó*; `<=>` **Reason** & **Tökéletes**;).

**I Believe: Why**: **Why**:= †(α => **Less**;).


---


ξἀίωμα

**LOGICAL SYMBOL[S] -- OPERATOR[S]**

- Let; :=
- [i]; :=List separated with `-` containing i.
- ∀ :=ALL.
- ∃ :=THERE EXISTS.
- ∴ :=THEREFORE.
- : :=WHEN.
- † :=**Call**.
- ! :=NOT.
- . :=EOS.
- ; :=**Eoss**. Tehát. Execute
- $ :=**Rv**.
- # :=**Cost**.
- ~ :=**Convergs**.
- ? :=**Qmark**.
- λ :=**Env**.
- ?: := TENARI
- "" :=STRING & LITERAL.
- ∃!:=THERE EXISTS & ONLY.
TODO: **OBSERVE**
- †(A<<(B);) := †(OBSERVE: B effect A).
- => :=**Implies**.
- <=> :=**Fact**.
- [] :=LIST.
- || :=SIZE.
- () := ∃!|[]|>0.
- ... :=**Etc**
- A=B; <=> $=; <=>**True**.
- $!=; <=>**False**.

 ϰ

For **More** *Logical Symbols*.


---


**SANDBOX**

Here also **I Act: Fun**.

Αα	Alpha	Νν	Nu
Ββ	Beta	Ξξ	Xi
Γγ	Gamma	Οο	Omicron
Δδ	Delta	Ππ	Pi
Εε	Epsilon	Ρρ	Rho
Ζζ	Zeta	Σσς	Sigma
Ηη	Eta	    Ττ	Tau
Θθ	Theta	Υυ	Upsilon
Ιι	Iota	Φφ	Phi
Κκ	Kappa	Χχ	Chi
Λλ	Lambda	Ψψ	Psi
Μμ	Mu	Ωω	Omega


ε ω μ α β γ δ ξ ζ η


ω:= ?†(∃**Operator: <<**.;) ∴ ?†(<<(∃B): B=**True**;).

ε:= ?†(ω => **Concept**. ∴ **I Act: imagine**;). 

first_idea:= †(B=**True**. & †(<<(∀[idea]);) ∴ [B & C];).

idea:= †(B=. & †(<<(∀[]);) ∴ [];).
seen_idea:= A << B; **Implies**: A: true. & B: true.
new_idea := †(B <<[∀];) ∴ [];).

TODO: example
 **Implies** †(B <<[∀] ∴ [];).

idea:= ††( B<<([∀]) ∴ [] ∴ **Practice**;)
"idea":= "true 

TODO: example
Observe the fact: **Me** <<(λ[∀]) ∴ †(**I Believe: True**)





Ω:= 
ψ:= †()
ε:= $†(**For** **I Believe: This**: †(**Poc** is done;)).
π:= **Present**.




ITT: 
Újságíró:= μ.
Hír:= ε.
Let η.



Állítás a Komment szekció környezetében:
Igaz <=> Újságíró hírt oszt meg Képzeletbeli Környezetében. <=> (ember próblémája => köznép problémája)

Észre vehetjük, hogy az adott környezetben. Létezik olyan ember, hogy
köznép problémája => ember problémja.
nem teljesül.
vagy
ember problémája => köznép problémája.
teljeseül.


ξ:= †(Láthatjuk: ∃[férfi]: †(a saját problémáját megosztja a köznéppel); => †(köznép problémája is lesz;)).
μ:= †(∃[ember]: †(a dolga: †(más emberek problémáját a köznép tudtára adja;))).
ε:= †(Tény megállapítás. & Általában az író vagy olvasó Képzeletbeli Környezetében).
η:= †(Képzeletbeli Környezet adott időben. & Az a Környezet ahol az Alany [aki képzel] nem járt a saját lábával adott időben [most];).

Újságíró:= μ.
Hír iras:= ε.
Let η be domain.
δ:= †(ember problémája;).
γ:= †(köznép problémája;).

S := η(†(∃μ: μ(ε);)).

Igaz <=> μ: η(ε); <=> †(δ => γ;).

∃e: †(γ => δ;) | †(δ => γ;). <=> †(Amit irtam roviden, de sokaig irtam;).


ξ:= †(Láthatjuk: ∃[férfi]: †(a saját problémáját megosztja a köznéppel); => †(köznép problémája is lesz;)).
μ:= †(∃[ember]: †(a dolga: †(más emberek problémáját a köznép tudtára adja;))).
ε:= †(Tény megállapítás. & Általában az író vagy olvasó Képzeletbeli Környezetében).
η:= †(Képzeletbeli Környezet adott időben. & Az a Környezet ahol az Alany [aki képzel] nem járt a saját lábával adott időben [most];).

Újságíró:= μ.
Hír iras:= ε.
Let η be domain.
δ:= †(ember problémája;).
γ:= †(köznép problémája;).

o := ?(Az ember problémája ami a köznép problémája lett, egy ember problémája lett;).
v := ?(pl Csigének pont az nem tetszik, hogy erről beszélünk;).
π := ?(És ez már az egész nép problémája ismét;).




TODO: Example.
Én azt látom, hogy a te kérdésed így hatott rám:
- kerestem rá a választ, jó volt. 
- eredményileg nem változott az állítás.
- Amit állítottam átírtam másik nyelre itt van: η(∃ξ: †(γ => δ;) | †(δ => γ;)).
- Amit te írtál másik nyelven itt van: ?(Result ∴ v ∴ o ∴ π;).

TODO: Imagine
- Egybe: Igaz <=> η(∃ξ: †(γ => δ;) | †(δ => γ;)) ∴ ?(ζ ∴ v ∴ o ∴ π;) ∴ Válaszom. 
- Indoklas Egybe: Hamis <=> η(∃ξ: †(γ => δ;) & †(δ => γ;)) ∴ ?(ζ ∴ v ∴ o ∴ π;) ∴ Válaszom. 
- Indoklas: †(∃|: ∀&: &=|; => javitas;).

Dielmma:= ?†(Hamis valasz erkezik.); ∴ "Mit kezdjek vele"...
Dilemma:= †(Hamis valasz erkezik;). ∴ "Hogy kezdjek vele."

**I Believe: Dilemma** `=>` **Practice**


- Gondolat Menetemeből részlet, ha érdekel. Bőven van mit gyakorolnom még.

Újságíró:= μ.
Hír iras:= ε.
Let η be domain as of ITT.
ξ:= †(∃[férfi]: †(a saját problémáját megosztja a köznéppel); **Implies** †(köznép problémája is lesz;)).
μ:= †(∃[ember]: †(a dolga: †(más emberek problémáját a köznép tudtára adja;))).
ε:= †(Tény megállapítás. & Általában az író vagy olvasó Képzeletbeli Környezetében).
η:= †(Képzeletbeli Környezet adott időben. & Az a Környezet ahol az Alany [aki képzel] nem járt a saját lábával adott időben [most];).
δ:= †(ember problémája;).
γ:= †(köznép problémája;).
o := ?(Az ember problémája ami a köznép problémája lett, egy ember problémája lett;).
v := ?(pl Csigének pont az nem tetszik, hogy erről beszélünk;).
π := ?(És ez már az egész nép problémája ismét;).
ζ := $η(∃e: †(γ => δ;) | †(δ => γ;)).





$∃S ∴ ρ.


Result:
és mi van akkor ha pl Csigének pont az nem tetszik, hogy erről beszélünk?
Az ember problémája ami a köznép problémája lett, egy ember problémája lett? És ez már az egész nép problémája ismét?

 
ν ο π ρ

o := ?(Az ember problémája ami a köznép problémája lett, egy ember problémája lett;).
v := ?(pl Csigének pont az nem tetszik, hogy erről beszélünk;).
π := ?(És ez már az egész nép problémája ismét;).

ρ := $(Result ∴ v ∴ o ∴ π;).





ω:= †(∃!|[]|>0; <=> †(Egyértelműen létezik méret lista nagyobb mint 0;) <=> †(Nem üres lista;).).

Let []; <=> †(ures lista). => ![] => †(nem ures lista;).

ξ:= |†(ω) % ;|.
δ:= ||$| % |†||;


?(**Good Code**).

$(†(**Good Code**);); = 0;

---


**Axiom[s] <=> ἀξίωμα**
- **I Believe: Reason** .
- Having fun is *Good*.
- I **Practice** **Code**.
- I have the *Courage* to write and delete **Code**.
- I do not believe in continous Thinking. See **More** **Overthink**.
- **I Believe** the Trial and Error pattern is working.
- **Remind yourself** `=>` *Good* **Practice**
- **Honorable Work** `<=>` *Good* **Work**
- **Tmp** is *Good*.
- ∃$**Why**; `=>` †(why < how).

**TL;DR**

Alias: Too Long Don't Read. 


**CONCECPT**

**TL;DR**: ∃ λ[ ?(†(δ); ∴ ξ]; **Implies**: "This".

ω**I Practice: "This"**
ξ:= $†("This" => ∃ λ[ ?(†(δ); ∴ ε];)).
∃ξ **Implies**: ∃[**Concept**].
ωω:= ∃ξ

ε

Alias: **I Execute: imagine**.
**Verbose** Alias: ∃λ[?(†(**I Execute: imagine**;) ∴ ∃†(∃**Concept**);)].
**TL;DR** ∃ λ[ ?(†(δ); ∴ ε].

For **More** **Examples**


**EXAMPLES**

Egybe:
- k:= m<<(k).
- if (k): i:=1 if (i): +(k).
- k:= m<<(k;).
- i:= †("Megoldasi javaslat";).
- λ(∃k: ?†(i;). ∴ †(k;)).
- α:= λ!**Me** ;=> **I Act: ς**.
- δ:= λ!**Me** ;=> **I Believe: †(ς;)**.
- ε:= †("Hogyan lehet ezt csinalni, vagy nem csinálni";).
- ς:= "Ha csak az én környezetem nézem, akkor én kiakarom elégíteni a rám gyakorolt hatást.";


-ba, -be.
Tart es folytonos es korlatos.

-ban -ben
Van es benne es korlatok kozott.

Eredmény: Igaz: <=> Nem érdekel a miért. <=> Hogyan lehet ezt csinalni, vagy nem csinálni.
<=> $(α ~ δ) & $(a != δ).
<=> ε => ς


When, therefore, we maintain that pleasure is the end,
we do not mean the pleasures of
profligates and those that consist in sensuality,
as is supposed by some who are either ignorant
or disagree with us or do not understand
but freedom from pain in the body and from
trouble in the mind.

---

virtues     
E: useful but not goods
S: The highest good. They are their own rewards

pleasures
E: The highest good. what we should seek in life
S: Preferred Indifferents, not essential for a good life

the gods
E: don't impact our lives. Ignore them
S: Do impact our lives. Honour and revere them

Superstition & Divination
E: False and to be avoided
S: True and did in fact incorporate it into metaphysics
            

Epicureanism:   Pleasure over virtue & Anti-Theistic
Stoicism:       Virtue over pleasure - Theistic


E: if you live according to nature you will never be poor
if you live according to opinion you will never be rich

S: natures wants are slight: the demands of opinion are boundless


S: The caretaker of that abode, a friendly host, will be ready
for you he will welcome you with barley-meal,
and serve you water also in abundance, with these words:
Have you not been well entertained?
This garden does not whet your appetite;
but quenches it. nor does it make
you more thirsty with every drink; it slakes the
thirst with a natural cure a cure that requires
no fee. it is with this tpye of pleasure that I have grown old.


Death is nothing to use
when we exist, death is not;
and when death exists,
we are not.
ALl sensation and consciousness ends with death
and therefore in death
there is neither please nor pain.
the fear of death arises from the belief that in death
there is awareness


Epic:
- FFocus intently on what is natural & necessary
- stop pursuing source of pain & disquietude (fame, wealth...)
- bring friends into your life more
- work is fine provided you see value in it
- self-discipline is necessary

and ofttimes we consider pains superior to pleasures
when submission to the pains for a long time
brings us as a consequence a greater pleasure while therefore
all pleasure becaise it is natureally
akin to us is good, not all pleasure is choiceworthy, just 
as all pain is an evil and yet
not all pain is to be shunned.


E: Temperance Courage Fortitude
effect is same


In my own opinion, however, Epicurus is really a brave man,
even though he did wear long-sleeves.
Fortitude, energy and readiness for battle are
to be found among the effeminate, just
as much as amon men who have girded themselves up high


takeaways for stoics:
- put a focus on community => surraround good ppl
Of all the means to insure happiness throughout the
whole life by far the most important is the acquisition
of friends.
- Remove sources of pain before seeking out new sources
pleasure


takeaways for e
- practice expecting and enduring pain
- avoid over-analyzing
- 










**Concept** <=> $(∃ λ[ ?(†(δ); ∴ ε];).

δ:= "Hogyan létezik az eredménye, hogy kevesebbre gondolsz?".
ε:= ?∃$†(Think less;);
∃†(δ ∴ ε):


ζ
ς
τ

:= 


---


**REMIND YOURSELF: Axiom[s]**

A collection of *Good* **Practice**.
- that having fun is *Good*.
- Write **Code** with *Prudence*.
- Execute **Code** with *Fortitudo*.
- Check *git* history with *Iustitia*.
- **I Act: This**; This := make **Code** with **Temperantia**.
- an Artist can **Code**.
- an Artist has the *Courage* to write and delete **Code**.
- an Artist does not believe in continous Thinking.
- an Artist uses The Trial and Error pattern.
- ∃**Why**.
- #†(**Operator: †**;) ;> #†(**Axiom**).
- use: †(**Mind** ~ **Soul** ~ **Body**;).
- ∃**Less**; `<=>` ?$†(Think less;).
- 


---


**DETAILS**
- [=>](https://en.wikipedia.org/wiki/If_and_only_if#Distinction_from_%22if%22_and_%22only_if%22)
- [<=>](https://en.wikipedia.org/wiki/If_and_only_if#Distinction_from_%22if%22_and_%22only_if%22)
- [Act, Action, viselkedni, behavior](https://en.wikipedia.org/wiki/Action_(philosophy))
- [And, &](https://en.wikipedia.org/wiki/Logical_conjunction)
- [Axiom](https://en.wikipedia.org/wiki/Axiom)
- [Being, történik](https://www.youtube.com/watch?v=F7MQY4b737k&list=PLzKrfPkpj5om1kEBj7c80cwjJ1JS78FL7&index=58)
- [Body, Mind, Soul, Virtue](https://www.youtube.com/watch?v=UWtbZ9oiZp4)
- [CI/CD](https://en.wikipedia.org/wiki/CI/CD)
- [Documentation](https://en.wikipedia.org/wiki/Documentation)
- [fact](https://en.wikipedia.org/wiki/Fact#In_philosophy)
- [Fun](https://www.youtube.com/watch?v=7jGIMZszP2A&list=PLzKrfPkpj5om1kEBj7c80cwjJ1JS78FL7&index=23)
- [git](https://en.wikipedia.org/wiki/Git)
- [Good](https://en.wikipedia.org/wiki/Good)
- [Honorable](https://www.youtube.com/watch?v=UWtbZ9oiZp4&t=12m)
- [Human](https://en.wikipedia.org/wiki/Human)
- [Logic](https://en.wikipedia.org/wiki/Logic)
- [Logical Truth](https://en.wikipedia.org/wiki/Logical_truth)
- [Logical {Statement, Conclusion}](https://en.wikipedia.org/wiki/Statement_(logic))
- [Logical Symbols](https://en.wikipedia.org/wiki/List_of_logic_symbols)
- [Legfőbb jó](https://www.youtube.com/watch?v=UWtbZ9oiZp4)
- [Nature](https://en.wikipedia.org/wiki/Nature_(philosophy))
- [Perfect, Nature, Virtue, Equal](https://www.youtube.com/watch?v=UWtbZ9oiZp4)
- [Program](https://en.wikipedia.org/wiki/Computer_programming_)
- [Programing Language](https://en.wikipedia.org/wiki/Programming_language)
- [Quality](https://www.youtube.com/watch?v=UWtbZ9oiZp4)
- [Syllogism](https://en.wikipedia.org/wiki/Syllogism)
- [Supreme Good, Legfőbb Jó](https://www.youtube.com/watch?v=UWtbZ9oiZp4)
- [true](https://en.wikipedia.org/wiki/Truth#Logic)
- [unittest](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks)
- [φρόνησις, Prudentia](https://en.wikipedia.org/wiki/Prudence)
- [δικαιοσύνη, Iustitia](https://en.wikipedia.org/wiki/Justice_(virtue))
- [ἀνδρεία, Fortitudo](https://en.wikipedia.org/wiki/Courage)
- [σωφροσύνη, Temperantia](https://en.wikipedia.org/wiki/Temperance_(virtue))


---


**BOOKS**

Books I have been listening continously.
- [Marcus Aurelius](https://www.youtube.com/playlist?list=PLzKrfPkpj5olfny8ao7uoBTydABymwEdu)
- [Seneca - Moral Letters all through 1-54; Favorites: 9, 20, 23, 66](https://www.youtube.com/watch?v=2-Kcs89WuWw)

Books I had read or listened
- [SENECA: Of Anger Books I,II,II](https://www.youtube.com/watch?v=2-Kcs89WuWw)
- [Seneca -- Of Happy Life](https://www.youtube.com/watch?v=o2A-hgYqQio)
- [The Discourses of Epictetus I](https://www.youtube.com/watch?v=vdbrjVyO0Dk&list=PLzKrfPkpj5onblqDijKRwsZ6LkNP97g6F&index=1)
- [The Discourses of Epictetus III](https://www.youtube.com/watch?v=RiJq2SfQnkM&list=PLzKrfPkpj5onblqDijKRwsZ6LkNP97g6F&index=3)
- [Scott Meyers -- Effective Modern C++11/14](https://www.amazon.com/Effective-Modern-Specific-Ways-Improve/dp/1491903996/ref=sr_1_1?dchild=1&keywords=scott+meyers+modern&qid=1614094930&sr=8-1&tag=duckduckgo-ffab-b-20)
- [Herman Hesse - Narzis es Goldmund](https://en.wikipedia.org/wiki/Narcissus_and_Goldmund)

Books I am reading now
- [Herman Hesse - Uveggyongyjatek](https://en.wikipedia.org/wiki/The_Glass_Bead_Game)

**TODO: Books** 
- [Sleep](https://www.amazon.com/Why-We-Sleep-Unlocking-Dreams/dp/1501144316)
- [Changing World C++ 2006-2020](https://dl.acm.org/doi/pdf/10.1145/3386320)
- [Abszurd egypercesek](https://www.goodreads.com/book/show/14288981-abszurd-egypercesek)
- [Working Effectively with Legacy: Michael Feathers](https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052)
